class CfgPatches
{
	class Chernarus_Utes_Config
	{
		units[] = {"Chernarus_Utes_Config"};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"Chernarus_Utes","Chernarus_Utes_Fixes","A3_Map_Data","A3_Data_F","CAData","CABuildings","CAMisc","CABuildings2","CARoads2","CAPlants","CARoads","CARocks"};
	};
};


#include "cfgSurfaces.cpp"
#include "cfgSurfaceCharacters.cpp"

#include "cfgWorld.cpp"
