class CfgWorlds
{
	class Chernarus;
	class Chernarus_Isles: Chernarus
	{
		
		worldName = "\x\chernarus_utes_c\chernarus_isles.wrp";
		pictureMap = "\x\chernarus_utes\sat_image.paa";
		pictureShot = "\x\chernarus_utes\sat_image.paa";
		description = "$STR_DN_CHERNARUS_ISLES";
		author = "$STR_a3_bohemia_interactive_modified_torndeco";
		class Names
		{
			#include "cfgNamesChernarus.cpp"
			#include "cfgNamesUtes.cpp"
		};
		class SecondaryAirports
		{
			#include "cfgAirportsChernarus.cpp"
			#include "cfgAirportsUtes.cpp"
		};
	};
};

#include "cfgWorldList.cpp"
#include "cfgLensFlare.cpp"
