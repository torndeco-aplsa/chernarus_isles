class city_Chernogorsk
{
	name = "$STR_LOCATION_CHERNOGORSK";
	position[] = {6731.21,2554.13};
	type = "NameCityCapital";
	speech[] = {"Chernogorsk"};
	radiusA = 300;
	radiusB = 300;
	angle = 0;
};
class city_Elektrozavodsk
{
	name = "$STR_LOCATION_ELEKTROZAVODSK";
	position[] = {10313.72,2159.41};
	type = "NameCity";
	speech[] = {"Elektrozavodsk"};
	radiusA = 200;
	radiusB = 200;
	angle = 0;
};
class vill_Tulga
{
	name = "$STR_LOCATION_TULGA";
	position[] = {12802.46,4369.49};
	type = "NameVillage";
	speech[] = {"Tulga"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class city_Solnychniy
{
	name = "$STR_LOCATION_SOLNICHNIY";
	position[] = {13397.41,6336.84};
	type = "NameCity";
	speech[] = {"Solnichniy"};
	radiusA = 200;
	radiusB = 200;
	angle = 0;
};
class city_Berezino
{
	name = "$STR_LOCATION_BEREZINO";
	position[] = {12014.01,9090.84};
	type = "NameCity";
	speech[] = {"Berezino"};
	radiusA = 200;
	radiusB = 200;
	angle = 0;
};
class city_StarySobor
{
	name = "$STR_LOCATION_STARYSOBOR";
	position[] = {6169.01,7793.67};
	type = "NameCity";
	speech[] = {"StarySobor"};
	radiusA = 200;
	radiusB = 200;
	angle = 0;
};
class vill_Msta
{
	name = "$STR_LOCATION_MSTA";
	position[] = {11333.79,5421.4};
	type = "NameVillage";
	speech[] = {"Msta"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Staroye
{
	name = "$STR_LOCATION_STAROYE";
	position[] = {10212.52,5385.04};
	type = "NameVillage";
	speech[] = {"Staroye"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Shakhovka
{
	name = "$STR_LOCATION_SHAKHOVKA";
	position[] = {9728.64,6567.3};
	type = "NameVillage";
	speech[] = {"Shakhovka"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Dolina
{
	name = "$STR_LOCATION_DOLINA";
	position[] = {11285.17,6633.58};
	type = "NameVillage";
	speech[] = {"Dolina"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Orlovets
{
	name = "$STR_LOCATION_ORLOVETS";
	position[] = {12131,7189.79};
	type = "NameVillage";
	speech[] = {"Orlovets"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Bor
{
	name = "$STR_LOCATION_BOR";
	position[] = {3326.74,3886.73};
	type = "NameVillage";
	speech[] = {"Bor"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class city_Gorka
{
	name = "$STR_LOCATION_GORKA";
	position[] = {9616.06,8914.38};
	type = "NameCity";
	speech[] = {"Gorka"};
	radiusA = 200;
	radiusB = 200;
	angle = 0;
};
class vill_NovySobor
{
	name = "$STR_LOCATION_NOVYSOBOR";
	position[] = {7123.63,7774.63};
	type = "NameVillage";
	speech[] = {"NovySobor"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Kabanino
{
	name = "$STR_LOCATION_KABANINO";
	position[] = {5300.71,8645.9};
	type = "NameVillage";
	speech[] = {"Kabanino"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Mogilevka
{
	name = "$STR_LOCATION_MOGILEVKA";
	position[] = {7650.41,5096.46};
	type = "NameVillage";
	speech[] = {"Mogilevka"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Nadezhdino
{
	name = "$STR_LOCATION_NADEZHDINO";
	position[] = {5876.66,4696.93};
	type = "NameVillage";
	speech[] = {"Nadezhdino"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class hill_Pik_Kozlova
{
	name = "$STR_LOCATION_MT_PIKKOZLOVA";
	position[] = {8850.17,2880.53};
	type = "Hill";
	radiusA = 50;
	radiusB = 50;
	angle = 0;
};
class hill_Altar
{
	name = "$STR_LOCATION_MT_ALTAR";
	position[] = {8137.82,9139.1};
	type = "Hill";
	radiusA = 50;
	radiusB = 50;
	angle = 0;
};
class hill_Ridge_Postoy
{
	name = "$STR_LOCATION_MT_RIDGEPOSTOY";
	position[] = {10725.2,5670.67};
	type = "Hill";
	radiusA = 50;
	radiusB = 50;
	angle = 0;
};
class vill_Guglovo
{
	name = "$STR_LOCATION_GUGLOVO";
	position[] = {8431.08,6703.19};
	type = "NameVillage";
	speech[] = {"Guglovo"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Kamyshovo
{
	name = "$STR_LOCATION_KAMYSHOVO";
	position[] = {12063.1,3515.58};
	type = "NameVillage";
	speech[] = {"Kamyshovo"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Pusta
{
	name = "$STR_LOCATION_PUSTA";
	position[] = {9127.18,3954.47};
	type = "NameVillage";
	speech[] = {"Pusta"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class city_Zelenogorsk
{
	name = "$STR_LOCATION_ZELENOGORSK";
	position[] = {2774.26,5380.38};
	type = "NameCity";
	speech[] = {"Zelenogorsk"};
	radiusA = 200;
	radiusB = 200;
	angle = 0;
};
class city_Vybor
{
	name = "$STR_LOCATION_VYBOR";
	position[] = {3833.17,8918.92};
	type = "NameCity";
	speech[] = {"Vybor"};
	radiusA = 200;
	radiusB = 200;
	angle = 0;
};
class vill_Dubrovka
{
	name = "$STR_LOCATION_DUBROVKA";
	position[] = {10382.13,9938.35};
	type = "NameVillage";
	speech[] = {"Dubrovka"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Khelm
{
	name = "$STR_LOCATION_KHELM";
	position[] = {12329.27,10771.01};
	type = "NameVillage";
	speech[] = {"Khelm"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Olsha
{
	name = "$STR_LOCATION_OLSHA";
	position[] = {13331.05,12900.05};
	type = "NameVillage";
	speech[] = {"Olsha"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Gvozdno
{
	name = "$STR_LOCATION_GVOZDNO";
	position[] = {8614.62,11890.32};
	type = "NameVillage";
	speech[] = {"Gvozdno"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class city_Krasnostav
{
	name = "$STR_LOCATION_KRASNOSTAV";
	position[] = {11224.26,12274.52};
	type = "NameCity";
	speech[] = {"Krasnostav"};
	radiusA = 200;
	radiusB = 200;
	angle = 0;
};
class vill_Petrovka
{
	name = "$STR_LOCATION_PETROVKA";
	position[] = {5016.45,12490.75};
	type = "NameVillage";
	speech[] = {"Petrovka"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Grishino
{
	name = "$STR_LOCATION_GRISHINO";
	position[] = {5959.94,10335.67};
	type = "NameVillage";
	speech[] = {"Grishino"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class Local_Skalka
{
	name = "$STR_LOCATION_LOC_SKALKA";
	position[] = {2060.16,12809.33};
	type = "NameLocal";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Rogovo
{
	name = "$STR_LOCATION_ROGOVO";
	position[] = {4770.96,6835.26};
	type = "NameVillage";
	speech[] = {"Rogovo"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Pogorevka
{
	name = "$STR_LOCATION_POGOREVKA";
	position[] = {4435.36,6425.58};
	type = "NameVillage";
	speech[] = {"Pogorevka"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Pustoshka
{
	name = "$STR_LOCATION_PUSTOSHKA";
	position[] = {3070.23,7940.48};
	type = "NameVillage";
	speech[] = {"Pustoshka"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Kozlovka
{
	name = "$STR_LOCATION_KOZLOVKA";
	position[] = {4477.57,4597.79};
	type = "NameVillage";
	speech[] = {"Kozlovka"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Polana
{
	name = "$STR_LOCATION_POLANA";
	position[] = {10707.06,8053.29};
	type = "NameVillage";
	speech[] = {"Polana"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Balota
{
	name = "$STR_LOCATION_BALOTA";
	position[] = {4527.58,2465.32};
	type = "NameVillage";
	speech[] = {"Balota"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Komarovo
{
	name = "$STR_LOCATION_KOMAROVO";
	position[] = {3664.31,2499};
	type = "NameVillage";
	speech[] = {"Komarovo"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Kamenka
{
	name = "$STR_LOCATION_KAMENKA";
	position[] = {1853.09,2249.85};
	type = "NameVillage";
	speech[] = {"Kamenka"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Myshkino
{
	name = "$STR_LOCATION_MYSHKINO";
	position[] = {1998.18,7355.13};
	type = "NameVillage";
	speech[] = {"Myshkino"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Pavlovo
{
	name = "$STR_LOCATION_PAVLOVO";
	position[] = {1693.98,3858.65};
	type = "NameVillage";
	speech[] = {"Pavlovo"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class Local_Dichina
{
	name = "$STR_LOCATION_LOC_DICHINA";
	position[] = {4626.16,7906.55};
	type = "NameLocal";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class Local_Devils_Castle
{
	name = "$STR_LOCATION_LOC_DEVILSCASTLE";
	position[] = {6816.39,11551.59};
	type = "NameLocal";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class Local_Zub
{
	name = "$STR_LOCATION_LOC_ZUB";
	position[] = {6574.28,5573.85};
	type = "NameLocal";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class Local_Rog
{
	name = "$STR_LOCATION_LOC_ROG";
	position[] = {11221.86,4353.38};
	type = "NameLocal";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class Local_Power_Plant
{
	name = "$STR_LOCATION_POWERPLANT";
	position[] = {10254.97,2639.05};
	type = "NameLocal";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Lopatino
{
	name = "$STR_LOCATION_LOPATINO";
	position[] = {2750,10004.98};
	type = "NameVillage";
	speech[] = {"Lopatino"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Vyshnoye
{
	name = "$STR_LOCATION_VYSHNOE";
	position[] = {6561.91,6078.77};
	type = "NameVillage";
	speech[] = {"Vyshnoe"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class vill_Prigorodki
{
	name = "$STR_LOCATION_PRIGORODKI";
	position[] = {8015.58,3419.26};
	type = "NameVillage";
	speech[] = {"Prigorodki"};
	radiusA = 144.91;
	radiusB = 81.43;
	angle = 0;
};
class vill_Drozhino
{
	name = "$STR_LOCATION_DROZHINO";
	position[] = {3382.02,4925.88};
	type = "NameVillage";
	speech[] = {"Drozhino"};
	radiusA = 57.81;
	radiusB = 66.94;
	angle = 0;
};
class vill_Sosnovka
{
	name = "$STR_LOCATION_SOSNOVKA";
	position[] = {2539.26,6358.71};
	type = "NameVillage";
	speech[] = {"Sosnovka"};
	radiusA = 57.46;
	radiusB = 62.62;
	angle = 0;
};
class vill_Nizhnoye
{
	name = "$STR_LOCATION_NIZHNOYE";
	position[] = {12824.88,8097.7};
	type = "NameVillage";
	speech[] = {"Nizhnoye"};
	radiusA = 97.78;
	radiusB = 111.66;
	angle = 0;
};
class vill_Pulkovo
{
	name = "$STR_LOCATION_PULKOVO";
	position[] = {4950.82,5627.1};
	type = "NameVillage";
	speech[] = {"Pulkovo"};
	radiusA = 60.01;
	radiusB = 48.11;
	angle = 0;
};
class AStrong_Chernogorsk_01
{
	name = "";
	position[] = {6329.81,2867.8};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 290;
};
class AStrong_Chernogorsk_02
{
	name = "";
	position[] = {6478.44,3036.28};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 330;
};
class AStrong_Chernogorsk_03
{
	name = "";
	position[] = {7181.44,3314.53};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 90;
};
class AStrong_Chernogorsk_04
{
	name = "";
	position[] = {7318.47,2691.98};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 40;
};
class AStrong_Chernogorsk_05
{
	name = "";
	position[] = {6486.8,2233.07};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 180;
};
class AStrong_Chernogorsk_06
{
	name = "";
	position[] = {6311.8,2253.6};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 270;
};
class AStrong_Chernogorsk_07
{
	name = "";
	position[] = {6013.38,2170.94};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 260;
};
class AFlatC_Chernogorsk
{
	name = "";
	position[] = {6881.44,2781.03};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 15;
};
class AFlat_001
{
	name = "";
	position[] = {5811.59,2081.12};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_002
{
	name = "";
	position[] = {6632.68,3550.57};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_003
{
	name = "";
	position[] = {7330.57,3404.08};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlatCS_Chernogorsk_01
{
	name = "";
	position[] = {6504.64,2403.03};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 130;
};
class AFlatCS_Chernogorsk_02
{
	name = "";
	position[] = {6994.2,2605.59};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 50;
};
class AFlatCS_Chernogorsk_03
{
	name = "";
	position[] = {6662.41,2976.61};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 325;
};
class AFlat_004
{
	name = "";
	position[] = {4925.81,2154.93};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AStrong_Balota_01
{
	name = "";
	position[] = {4363.75,2299.27};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 300;
};
class AStrong_Balota_02
{
	name = "";
	position[] = {4357.23,2561.82};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 300;
};
class AStrong_Balota_03
{
	name = "";
	position[] = {4613.53,2565.56};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 350;
};
class AStrong_Balota_04
{
	name = "";
	position[] = {4607.47,2405.78};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 90;
};
class ACityC_Chernogorsk
{
	name = "";
	position[] = {6735.83,2626.6};
	type = "CityCenter";
	neighbors[] = {"ACityC_Prigorodki","ACityC_Balota","ACityC_Nadezhdino","ACityC_Mogilevka"};
	demography[] = {"CIV",1,"CIV_RU",0};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Balota
{
	name = "";
	position[] = {4498.07,2449.39};
	type = "CityCenter";
	neighbors[] = {"ACityC_Chernogorsk","ACityC_Komarovo","ACityC_Kozlovka"};
	demography[] = {"CIV",1,"CIV_RU",0};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlatCS_Balota_01
{
	name = "";
	position[] = {4591.08,2450.67};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 10;
};
class AFlat_005
{
	name = "";
	position[] = {4063.19,2850.32};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_006
{
	name = "";
	position[] = {3177.66,2047.28};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_007
{
	name = "";
	position[] = {1173.92,2445.61};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_008
{
	name = "";
	position[] = {2014.02,2848.63};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_009
{
	name = "";
	position[] = {2670.98,3375.57};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_010
{
	name = "";
	position[] = {2051.36,4343.92};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_011
{
	name = "";
	position[] = {2107.03,5051.17};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_012
{
	name = "";
	position[] = {3129.51,4469.73};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_013
{
	name = "";
	position[] = {3408.55,5433.56};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_014
{
	name = "";
	position[] = {4053.07,4864.02};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_015
{
	name = "";
	position[] = {2330.27,5980.01};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_016
{
	name = "";
	position[] = {2869.66,6690.13};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_017
{
	name = "";
	position[] = {3339.92,6597.18};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_018
{
	name = "";
	position[] = {3264.86,7266.2};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_019
{
	name = "";
	position[] = {3766.45,8114.37};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_020
{
	name = "";
	position[] = {3325.96,8736.5};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_021
{
	name = "";
	position[] = {2735.22,9423.34};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_022
{
	name = "";
	position[] = {3732.01,10153.9};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_023
{
	name = "";
	position[] = {2192.24,10800.42};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_024
{
	name = "";
	position[] = {3865.45,11133.41};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_025
{
	name = "";
	position[] = {4529.94,9147.24};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_026
{
	name = "";
	position[] = {5315.17,10960.31};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_027
{
	name = "";
	position[] = {5810.39,11338.28};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_028
{
	name = "";
	position[] = {5610.61,12283.28};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_029
{
	name = "";
	position[] = {7476.93,11361.01};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_030
{
	name = "";
	position[] = {8283.83,10607.15};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_031
{
	name = "";
	position[] = {9089.95,11354};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_032
{
	name = "";
	position[] = {9418.09,12168.5};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_033
{
	name = "";
	position[] = {10390.41,12470.42};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_034
{
	name = "";
	position[] = {10933.36,11472.3};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_035
{
	name = "";
	position[] = {12318.56,13055.08};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_036
{
	name = "";
	position[] = {13122.77,11274.8};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_037
{
	name = "";
	position[] = {13170.5,10722.49};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_038
{
	name = "";
	position[] = {11813.95,9903.66};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_039
{
	name = "";
	position[] = {12653.99,9031.65};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_040
{
	name = "";
	position[] = {12163.5,8549.9};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_041
{
	name = "";
	position[] = {13137.62,7470};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_042
{
	name = "";
	position[] = {13442.9,5248.4};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_043
{
	name = "";
	position[] = {12880.45,5582.35};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_044
{
	name = "";
	position[] = {12597.94,3598.7};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_045
{
	name = "";
	position[] = {11180.99,4754.65};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_046
{
	name = "";
	position[] = {11773.28,5662.78};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_047
{
	name = "";
	position[] = {10007.46,6010.35};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_048
{
	name = "";
	position[] = {10694.88,6889.37};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 320;
};
class AFlat_049
{
	name = "";
	position[] = {11150.3,7874.84};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_050
{
	name = "";
	position[] = {10481.09,8641.44};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_051
{
	name = "";
	position[] = {10896.81,9058.49};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_052
{
	name = "";
	position[] = {9987.1,10544.83};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_053
{
	name = "";
	position[] = {10133.48,8392.46};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_054
{
	name = "";
	position[] = {7253.13,9838.1};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_055
{
	name = "";
	position[] = {8174.12,9026.43};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_056
{
	name = "";
	position[] = {7958.59,8264.05};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_057
{
	name = "";
	position[] = {8517.94,7973.78};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_058
{
	name = "";
	position[] = {8850.2,6330.11};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_059
{
	name = "";
	position[] = {8215.92,5800.98};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_060
{
	name = "";
	position[] = {7857.14,7154.31};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_061
{
	name = "";
	position[] = {5610.09,8205.99};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_062
{
	name = "";
	position[] = {4865.21,8087.61};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_063
{
	name = "";
	position[] = {4523.31,7190.4};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_064
{
	name = "";
	position[] = {5569.96,7228.22};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_065
{
	name = "";
	position[] = {6247.75,6869.7};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_066
{
	name = "";
	position[] = {6943.65,5718.2};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_067
{
	name = "";
	position[] = {5775.64,9179.84};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_068
{
	name = "";
	position[] = {6672.03,8983.22};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_069
{
	name = "";
	position[] = {5015.76,3667.38};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_070
{
	name = "";
	position[] = {8520.4,2681.92};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_071
{
	name = "";
	position[] = {6542,4730.7};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_072
{
	name = "";
	position[] = {7772.44,4483.84};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_073
{
	name = "";
	position[] = {9103.76,3447.82};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_074
{
	name = "";
	position[] = {8744.62,2275};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlat_075
{
	name = "";
	position[] = {11088.91,3051.86};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AStrong_Komarovo_01
{
	name = "";
	position[] = {3498.06,2502.13};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 290;
};
class AStrong_Komarovo_02
{
	name = "";
	position[] = {3421.86,2355.14};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 230;
};
class AStrong_Komarovo_03
{
	name = "";
	position[] = {3610,2277.04};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 90;
};
class AStrong_Komarovo_04
{
	name = "";
	position[] = {3813.88,2539.62};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 50;
};
class AStrong_Kamenka_01
{
	name = "";
	position[] = {2174.05,2231.89};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 120;
};
class AStrong_Kamenka_02
{
	name = "";
	position[] = {1574.21,2225.27};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 310;
};
class AStrong_Kamenka_03
{
	name = "";
	position[] = {1738.02,2364.54};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 5;
};
class AStrong_Kamenka_04
{
	name = "";
	position[] = {1985.02,2169.76};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 175;
};
class AStrong_Pavlovo_01
{
	name = "";
	position[] = {1772.95,3578.54};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 180;
};
class AStrong_Pavlovo_02
{
	name = "";
	position[] = {1917.22,3801.09};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 90;
};
class AStrong_Pavlovo_03
{
	name = "";
	position[] = {1818.71,4089.96};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AStrong_Zelenogorsk_01
{
	name = "";
	position[] = {2468.85,4991.24};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 215;
};
class AStrong_Zelenogorsk_02
{
	name = "";
	position[] = {2451.34,5229.32};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 285;
};
class AStrong_Zelenogorsk_03
{
	name = "";
	position[] = {2710.61,5778.91};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 350;
};
class AStrong_Zelenogorsk_04
{
	name = "";
	position[] = {3191.54,5566.72};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 90;
};
class AStrong_Zelenogorsk_05
{
	name = "";
	position[] = {3020.77,5257.98};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 120;
};
class AStrong_Bor_01
{
	name = "";
	position[] = {3229.91,4270.79};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 360;
};
class AStrong_Bor_02
{
	name = "";
	position[] = {3342.4,3823.52};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 190;
};
class AStrong_Bor_03
{
	name = "";
	position[] = {3589.89,3915.1};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 125;
};
class AStrong_Kozlovka_01
{
	name = "";
	position[] = {4073.13,4669.06};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 140;
};
class AStrong_Kozlovka_02
{
	name = "";
	position[] = {4126.82,4884.66};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 235;
};
class AStrong_Kozlovka_03
{
	name = "";
	position[] = {4740.74,4751.81};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 25;
};
class AStrong_Kozlovka_04
{
	name = "";
	position[] = {4557.41,4372.1};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 120;
};
class AStrong_Pogorevka_01
{
	name = "";
	position[] = {4274.78,6518.44};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 310;
};
class AStrong_Pogorevka_02
{
	name = "";
	position[] = {4437.05,6287.04};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 195;
};
class AStrong_Pogorevka_03
{
	name = "";
	position[] = {4591.23,6467.21};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 145;
};
class AStrong_Rogovo_01
{
	name = "";
	position[] = {4799.27,6678.24};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 190;
};
class AStrong_Rogovo_02
{
	name = "";
	position[] = {4860.91,6815.34};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 105;
};
class AStrong_Rogovo_03
{
	name = "";
	position[] = {4863.62,7073.8};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 330;
};
class AStrong_Rogovo_04
{
	name = "";
	position[] = {4581.29,6928.57};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 310;
};
class AStrong_Pustoshka_01
{
	name = "";
	position[] = {3017.85,7602.57};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 190;
};
class AStrong_Pustoshka_02
{
	name = "";
	position[] = {3298.03,7913.21};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 100;
};
class AStrong_Pustoshka_03
{
	name = "";
	position[] = {3250.01,8262.25};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 65;
};
class AStrong_Pustoshka_04
{
	name = "";
	position[] = {2969.7,8044.09};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 335;
};
class AStrong_Vybor_01
{
	name = "";
	position[] = {3625.18,8511.43};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 235;
};
class AStrong_Vybor_02
{
	name = "";
	position[] = {3600.31,9074.23};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 300;
};
class AStrong_Vybor_03
{
	name = "";
	position[] = {4054.09,8757.92};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 90;
};
class AStrong_Lopatino_01
{
	name = "";
	position[] = {2944.76,9715.82};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 130;
};
class AStrong_Lopatino_02
{
	name = "";
	position[] = {2553.58,9836.94};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 175;
};
class AStrong_Lopatino_03
{
	name = "";
	position[] = {2633.66,10202.67};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 295;
};
class AStrong_Lopatino_04
{
	name = "";
	position[] = {2945.79,9936.53};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 60;
};
class AStrong_AirField_01
{
	name = "";
	position[] = {4083.95,10287.39};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 230;
};
class AStrong_AirField_02
{
	name = "";
	position[] = {4522.24,9793.23};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 180;
};
class AStrong_AirField_03
{
	name = "";
	position[] = {4465.91,9498.5};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 210;
};
class AStrong_AirField_04
{
	name = "";
	position[] = {5058.49,9670.99};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 30;
};
class AStrong_AirField_05
{
	name = "";
	position[] = {4826.65,10294.78};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 330;
};
class AStrong_AirField_06
{
	name = "";
	position[] = {4484.99,10867.97};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 120;
};
class AStrong_AirField_07
{
	name = "";
	position[] = {4110.95,11125.75};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AStrong_Grishino_01
{
	name = "";
	position[] = {5858.96,10075.26};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 180;
};
class AStrong_Grishino_02
{
	name = "";
	position[] = {5968.55,10062.81};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 150;
};
class AStrong_Grishino_03
{
	name = "";
	position[] = {5705.31,10251.39};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 300;
};
class AStrong_Grishino_04
{
	name = "";
	position[] = {6072.27,10598.68};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 20;
};
class AStrong_Grishino_05
{
	name = "";
	position[] = {6247.73,10397.87};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 100;
};
class AStrong_Gvozdno_01
{
	name = "";
	position[] = {8287.87,12081.69};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 240;
};
class AStrong_Gvozdno_02
{
	name = "";
	position[] = {8708.44,11603.87};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 275;
};
class AStrong_Gvozdno_03
{
	name = "";
	position[] = {9061.04,11774.48};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 100;
};
class AStrong_Gvozdno_04
{
	name = "";
	position[] = {8794.4,11903.68};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 100;
};
class AStrong_Krasnostav_01
{
	name = "";
	position[] = {10642.85,12384.82};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 275;
};
class AStrong_Krasnostav_02
{
	name = "";
	position[] = {11181.38,12067.77};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 220;
};
class AStrong_Krasnostav_03
{
	name = "";
	position[] = {11761.73,12281.27};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 90;
};
class AStrong_Krasnostav_04
{
	name = "";
	position[] = {11628.68,12546.44};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 30;
};
class AStrong_Krasnostav_05
{
	name = "";
	position[] = {11166.36,12507.27};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 50;
};
class AStrong_Olsha_01
{
	name = "";
	position[] = {13247.15,12765.66};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 115;
};
class AStrong_Khelm_01
{
	name = "";
	position[] = {12091.99,10594.87};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 270;
};
class AStrong_Khelm_02
{
	name = "";
	position[] = {12378.01,10564.24};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 150;
};
class AStrong_Khelm_03
{
	name = "";
	position[] = {12373.51,11020.49};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 60;
};
class AStrong_Dubrovka_01
{
	name = "";
	position[] = {10878.25,9939.88};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 90;
};
class AStrong_Dubrovka_02
{
	name = "";
	position[] = {10249.98,9951.04};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 25;
};
class AStrong_Dubrovka_03
{
	name = "";
	position[] = {10504.58,10074.34};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AStrong_Dubrovka_04
{
	name = "";
	position[] = {10517.92,9578.36};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 115;
};
class AStrong_Dubrovka_05
{
	name = "";
	position[] = {10392.52,9537.81};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 190;
};
class AStrong_Berezino_01
{
	name = "";
	position[] = {11424.36,9388.34};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 270;
};
class AStrong_Berezino_02
{
	name = "";
	position[] = {11889.32,9354.62};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 320;
};
class AStrong_Berezino_03
{
	name = "";
	position[] = {11932.96,8701.13};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 120;
};
class AStrong_Berezino_04
{
	name = "";
	position[] = {12291.06,8924.34};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 90;
};
class AStrong_Berezino_05
{
	name = "";
	position[] = {12097.72,9292.37};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 300;
};
class AStrong_Polana_02
{
	name = "";
	position[] = {10887.91,8048.79};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 125;
};
class AStrong_Polana_01
{
	name = "";
	position[] = {10645.83,8317.94};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 360;
};
class AStrong_Polana_03
{
	name = "";
	position[] = {10625.96,7771.07};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 210;
};
class AStrong_Polana_04
{
	name = "";
	position[] = {10394.67,7760.1};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 280;
};
class AStrong_Gorka_01
{
	name = "";
	position[] = {9056.94,8889.79};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 300;
};
class AStrong_Gorka_02
{
	name = "";
	position[] = {9530.46,8970.73};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 60;
};
class AStrong_Gorka_03
{
	name = "";
	position[] = {9217.85,8755.51};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 185;
};
class AStrong_Gorka_04
{
	name = "";
	position[] = {9943.65,8850.86};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 80;
};
class AStrong_Gorka_05
{
	name = "";
	position[] = {9982.89,8663.2};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 180;
};
class AStrong_Orlovets_01
{
	name = "";
	position[] = {11918.56,7250.96};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 305;
};
class AStrong_Orlovets_02
{
	name = "";
	position[] = {12399.09,7370.42};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 360;
};
class AStrong_Dolina_01
{
	name = "";
	position[] = {11030.07,6704.28};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 275;
};
class AStrong_Dolina_02
{
	name = "";
	position[] = {11640.43,6583.65};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 90;
};
class AStrong_Solnichniy_01
{
	name = "";
	position[] = {13115.47,6256};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 270;
};
class AStrong_Solnichniy_02
{
	name = "";
	position[] = {13425.08,6460.38};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 350;
};
class AStrong_Solnichniy_03
{
	name = "";
	position[] = {13395.21,5932.02};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 180;
};
class AStrong_Shakhovka_01
{
	name = "";
	position[] = {9702.1,6248.43};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 70;
};
class AStrong_Shakhovka_02
{
	name = "";
	position[] = {9782.59,6543.7};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 85;
};
class AStrong_Shakhovka_03
{
	name = "";
	position[] = {9471.98,6808.37};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 75;
};
class AStrong_Shakhovka_04
{
	name = "";
	position[] = {9668.36,6779.11};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 10;
};
class AStrong_Staroye_01
{
	name = "";
	position[] = {10172.21,5228.89};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 150;
};
class AStrong_Staroye_02
{
	name = "";
	position[] = {9820.59,5467.55};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 270;
};
class AStrong_Staroye_03
{
	name = "";
	position[] = {9928.93,5927.96};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 340;
};
class AStrong_Staroye_04
{
	name = "";
	position[] = {10505.48,5480.98};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 75;
};
class AStrong_Staroye_05
{
	name = "";
	position[] = {10325.55,5734.03};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 50;
};
class AStrong_Tulga_01
{
	name = "";
	position[] = {12907.59,4457.43};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 280;
};
class AStrong_Tulga_02
{
	name = "";
	position[] = {12541.75,4552.67};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 60;
};
class AStrong_Tulga_03
{
	name = "";
	position[] = {12508.4,4462.73};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 260;
};
class AStrong_Kamyshovo_01
{
	name = "";
	position[] = {11852.03,3480.01};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 255;
};
class AStrong_Kamyshovo_02
{
	name = "";
	position[] = {12383.77,3552.52};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 85;
};
class AStrong_Kamyshovo_03
{
	name = "";
	position[] = {12004.6,3800.47};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 320;
};
class AStrong_Elektrozavodsk_01
{
	name = "";
	position[] = {10924.11,2779.45};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AStrong_Elektrozavodsk_02
{
	name = "";
	position[] = {10379.75,1971.46};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 180;
};
class AStrong_Elektrozavodsk_03
{
	name = "";
	position[] = {10383.54,2718.41};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 330;
};
class AStrong_Elektrozavodsk_04
{
	name = "";
	position[] = {10196.27,2358.27};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 240;
};
class AStrong_Elektrozavodsk_05
{
	name = "";
	position[] = {9555.84,2250.67};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 70;
};
class AStrong_Elektrozavodsk_06
{
	name = "";
	position[] = {9868.6,1894.98};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 240;
};
class AStrong_Elektrozavodsk_07
{
	name = "";
	position[] = {9182.6,2101.25};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 280;
};
class AStrong_Pusta_01
{
	name = "";
	position[] = {9126.34,3620.25};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 95;
};
class AStrong_Pusta_02
{
	name = "";
	position[] = {8843.86,4021.92};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 315;
};
class AStrong_Pusta_03
{
	name = "";
	position[] = {9362.76,4025.28};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 20;
};
class AStrong_Mogilevka_01
{
	name = "";
	position[] = {7522.63,4706.67};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 180;
};
class AStrong_Mogilevka_02
{
	name = "";
	position[] = {7592.56,4915.05};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 105;
};
class AStrong_Mogilevka_03
{
	name = "";
	position[] = {7432.33,5104.58};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 290;
};
class AStrong_Mogilevka_04
{
	name = "";
	position[] = {7362.05,5292.25};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 335;
};
class AStrong_Mogilevka_05
{
	name = "";
	position[] = {7802.83,5218.96};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 5;
};
class AStrong_Vyshnoye_01
{
	name = "";
	position[] = {6638.91,5857.39};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 110;
};
class AStrong_Vyshnoye_02
{
	name = "";
	position[] = {6348.21,6241.24};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 310;
};
class AStrong_Vyshnoye_03
{
	name = "";
	position[] = {6602.14,6204.11};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 330;
};
class AStrong_NovySobor_01
{
	name = "";
	position[] = {7351.73,7694.96};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 140;
};
class AStrong_NovySobor_02
{
	name = "";
	position[] = {6911.07,7856.05};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 20;
};
class AStrong_NovySobor_03
{
	name = "";
	position[] = {6880.22,7579.58};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 10;
	angle = 300;
};
class AStrong_NovySobor_04
{
	name = "";
	position[] = {7086.97,7438.21};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 125;
};
class AStrong_StarySobor_01
{
	name = "";
	position[] = {6373.77,7555.69};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 160;
};
class AStrong_StarySobor_02
{
	name = "";
	position[] = {5855.67,7529.08};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 200;
};
class AStrong_StarySobor_03
{
	name = "";
	position[] = {6257.96,7839.42};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 330;
};
class AStrong_StarySobor_04
{
	name = "";
	position[] = {6243.91,8026.06};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 60;
};
class AStrong_StarySobor_05
{
	name = "";
	position[] = {5824.87,8121.5};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 325;
};
class AStrong_Kabanino_01
{
	name = "";
	position[] = {5532.61,8397.47};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 140;
};
class AStrong_Kabanino_02
{
	name = "";
	position[] = {5169.41,8542.31};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 300;
};
class AStrong_Kabanino_03
{
	name = "";
	position[] = {5277.36,8765.71};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 330;
};
class AStrong_Kabanino_04
{
	name = "";
	position[] = {5488.48,8712.34};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 50;
};
class AStrong_Bor_04
{
	name = "";
	position[] = {2795.24,4119.73};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 15;
};
class AStrong_Nadezhdino_01
{
	name = "";
	position[] = {5796.27,4825.3};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 295;
};
class AStrong_Nadezhdino_02
{
	name = "";
	position[] = {6025.22,4886.99};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 115;
};
class AStrong_Nadezhdino_03
{
	name = "";
	position[] = {5799.28,5036.47};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 15;
};
class AStrong_Nadezhdino_04
{
	name = "";
	position[] = {5800.69,4417.76};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 260;
};
class AStrong_Drozhino_01
{
	name = "";
	position[] = {3254.26,5052.21};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 315;
};
class AStrong_Drozhino_02
{
	name = "";
	position[] = {3525.2,4888.45};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 100;
};
class AStrong_Pulkovo_01
{
	name = "";
	position[] = {4833.52,5719.49};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 350;
};
class AStrong_Pulkovo_02
{
	name = "";
	position[] = {5018.95,5597.01};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 110;
};
class AStrong_Sosnovka_01
{
	name = "";
	position[] = {2818.84,6647.56};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 100;
};
class AStrong_Sosnovka_02
{
	name = "";
	position[] = {2501.56,6080.72};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 135;
};
class AStrong_Myshkino_01
{
	name = "";
	position[] = {2316.46,7201.92};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 40;
};
class AStrong_Myshkino_02
{
	name = "";
	position[] = {1868.52,7547.36};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AStrong_Vybor_04
{
	name = "";
	position[] = {4113.06,9088.76};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 60;
};
class AStrong_Petrovka_01
{
	name = "";
	position[] = {4849.9,12570.37};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 175;
};
class AStrong_Petrovka_02
{
	name = "";
	position[] = {5105.77,12497.31};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 145;
};
class AStrong_Olsha_02
{
	name = "";
	position[] = {13306.23,12958.62};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 330;
};
class AStrong_Berezino_06
{
	name = "";
	position[] = {13141.91,10473.35};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 20;
};
class AStrong_Berezino_07
{
	name = "";
	position[] = {12816.15,10254.67};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 290;
};
class AStrong_Berezino_08
{
	name = "";
	position[] = {12678.79,9892.24};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 270;
};
class AStrong_Berezino_09
{
	name = "";
	position[] = {12913.97,9683.69};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 190;
};
class AStrong_Nizhnoye_01
{
	name = "";
	position[] = {12658.18,8395.95};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 360;
};
class AStrong_Nizhnoye_02
{
	name = "";
	position[] = {12933.78,8423.15};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 345;
};
class AStrong_Nizhnoye_03
{
	name = "";
	position[] = {12751.63,7996.58};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 210;
};
class AStrong_Nizhnoye_04
{
	name = "";
	position[] = {13063.24,7975.24};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 185;
};
class AStrong_Solnichniy_04
{
	name = "";
	position[] = {13145.09,7190.32};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 355;
};
class AStrong_Solnichniy_05
{
	name = "";
	position[] = {13137.03,6913.16};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 120;
};
class AStrong_Orlovets_03
{
	name = "";
	position[] = {12157.33,7400.25};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 65;
};
class AStrong_Orlovets_04
{
	name = "";
	position[] = {12182.57,6980.58};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 145;
};
class AStrong_Dolina_03
{
	name = "";
	position[] = {12041.27,6348.71};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 300;
};
class AStrong_Dolina_04
{
	name = "";
	position[] = {12424.51,6251.06};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 100;
};
class AStrong_Msta_01
{
	name = "";
	position[] = {11041.68,5347.4};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 5;
};
class AStrong_Msta_02
{
	name = "";
	position[] = {11079.78,5142.4};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 135;
};
class AStrong_Msta_03
{
	name = "";
	position[] = {11440.31,5497.8};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 90;
};
class AStrong_Tulga_04
{
	name = "";
	position[] = {12398.87,4376.36};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AStrong_Prigorodki_01
{
	name = "";
	position[] = {7579.7,3215.71};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 265;
};
class AStrong_Prigorodki_02
{
	name = "";
	position[] = {7989.64,3131.91};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 85;
};
class AStrong_Prigorodki_03
{
	name = "";
	position[] = {7637.42,3499.4};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 240;
};
class AStrong_Prigorodki_04
{
	name = "";
	position[] = {7880.12,3532.44};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 135;
};
class AStrong_Guglovo_01
{
	name = "";
	position[] = {8563.02,6589.22};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 105;
};
class AStrong_Guglovo_02
{
	name = "";
	position[] = {8295.73,6775.43};
	type = "StrongpointArea";
	radiusA = 100;
	radiusB = 100;
	angle = 315;
};
class AFlat_076
{
	name = "";
	position[] = {13180.61,3987.41};
	type = "FlatArea";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Komarovo
{
	name = "";
	position[] = {3670.81,2385.79};
	type = "CityCenter";
	neighbors[] = {"ACityC_Bor","ACityC_Pavlovo","ACityC_Kamenka","ACityC_Balota"};
	demography[] = {"CIV",1,"CIV_RU",0};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Kamenka
{
	name = "";
	position[] = {1860.74,2220.66};
	type = "CityCenter";
	neighbors[] = {"ACityC_Komarovo","ACityC_Pavlovo"};
	demography[] = {"CIV",1,"CIV_RU",0};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Pavlovo
{
	name = "";
	position[] = {1691.95,3838.99};
	type = "CityCenter";
	neighbors[] = {"ACityC_Bor","ACityC_Zelenogorsk","ACityC_Kamenka","ACityC_Komarovo"};
	demography[] = {"CIV",1,"CIV_RU",0};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Bor
{
	name = "";
	position[] = {3351.68,3950.19};
	type = "CityCenter";
	neighbors[] = {"ACityC_Pavlovo","ACityC_Kozlovka","ACityC_Drozhino","ACityC_Komarovo"};
	demography[] = {"CIV",1,"CIV_RU",0};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Zelenogorsk
{
	name = "";
	position[] = {2754.43,5270.98};
	type = "CityCenter";
	neighbors[] = {"ACityC_Drozhino","ACityC_Pulkovo","ACityC_Pavlovo","ACityC_Sosnovka","ACityC_Pogorevka"};
	demography[] = {"CIV",0.9,"CIV_RU",0.1};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Drozhino
{
	name = "";
	position[] = {3379.62,4922.15};
	type = "CityCenter";
	neighbors[] = {"ACityC_Bor","ACityC_Kozlovka","ACityC_Zelenogorsk","ACityC_Pulkovo"};
	demography[] = {"CIV",1,"CIV_RU",0};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Kozlovka
{
	name = "";
	position[] = {4467.61,4626.55};
	type = "CityCenter";
	neighbors[] = {"ACityC_Drozhino","ACityC_Balota","ACityC_Bor","ACityC_Nadezhdino"};
	demography[] = {"CIV",1,"CIV_RU",0};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Sosnovka
{
	name = "";
	position[] = {2527.6,6336.11};
	type = "CityCenter";
	neighbors[] = {"ACityC_Pustoshka","ACityC_Myshkino","ACityC_Zelenogorsk","ACityC_Pogorevka"};
	demography[] = {"CIV",0.9,"CIV_RU",0.1};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Pulkovo
{
	name = "";
	position[] = {4931.55,5612.53};
	type = "CityCenter";
	neighbors[] = {"ACityC_Zelenogorsk","ACityC_Rogovo","ACityC_Pogorevka","ACityC_Drozhino"};
	demography[] = {"CIV",1,"CIV_RU",0};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Pogorevka
{
	name = "";
	position[] = {4464.15,6420.89};
	type = "CityCenter";
	neighbors[] = {"ACityC_Rogovo","ACityC_Zelenogorsk","ACityC_Pustoshka","ACityC_Pulkovo","ACityC_Sosnovka"};
	demography[] = {"CIV",1,"CIV_RU",0};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Rogovo
{
	name = "";
	position[] = {4742.94,6775.92};
	type = "CityCenter";
	neighbors[] = {"ACityC_Pogorevka","ACityC_Kabanino","ACityC_StarySobor","ACityC_Nadezhdino","ACityC_Pulkovo","ACityC_Vyshnoye"};
	demography[] = {"CIV",0.9,"CIV_RU",0.1};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Myshkino
{
	name = "";
	position[] = {2002.45,7322.61};
	type = "CityCenter";
	neighbors[] = {"ACityC_Pustoshka","ACityC_Sosnovka","ACityC_Lopatino"};
	demography[] = {"CIV",0.8,"CIV_RU",0.2};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Pustoshka
{
	name = "";
	position[] = {3048.06,7885.56};
	type = "CityCenter";
	neighbors[] = {"ACityC_Vybor","ACityC_Myshkino","ACityC_Pogorevka","ACityC_Sosnovka","ACityC_Kabanino","ACityC_Lopatino"};
	demography[] = {"CIV",0.8,"CIV_RU",0.2};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Vybor
{
	name = "";
	position[] = {3904.06,8932.71};
	type = "CityCenter";
	neighbors[] = {"ACityC_Lopatino","ACityC_Pustoshka","ACityC_Kabanino"};
	demography[] = {"CIV",0.7,"CIV_RU",0.3};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Lopatino
{
	name = "";
	position[] = {2767.78,9935.97};
	type = "CityCenter";
	neighbors[] = {"ACityC_Vybor","ACityC_Pustoshka","ACityC_Grishino","ACityC_Myshkino"};
	demography[] = {"CIV",0.3,"CIV_RU",0.7};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Kabanino
{
	name = "";
	position[] = {5347.5,8595.36};
	type = "CityCenter";
	neighbors[] = {"ACityC_Rogovo","ACityC_StarySobor","ACityC_Grishino","ACityC_Vybor","ACityC_Pustoshka"};
	demography[] = {"CIV",0.6,"CIV_RU",0.4};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Grishino
{
	name = "";
	position[] = {5980.06,10289.6};
	type = "CityCenter";
	neighbors[] = {"ACityC_Kabanino","ACityC_Petrovka","ACityC_NovySobor","ACityC_Gvozdno","ACityC_Gorka","ACityC_Lopatino"};
	demography[] = {"CIV",0.3,"CIV_RU",0.7};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Petrovka
{
	name = "";
	position[] = {4987.79,12510.57};
	type = "CityCenter";
	neighbors[] = {"ACityC_Grishino"};
	demography[] = {"CIV",0,"CIV_RU",1};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_StarySobor
{
	name = "";
	position[] = {6115.92,7747.75};
	type = "CityCenter";
	neighbors[] = {"ACityC_Kabanino","ACityC_NovySobor","ACityC_Rogovo","ACityC_Vyshnoye"};
	demography[] = {"CIV",0.7,"CIV_RU",0.3};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_NovySobor
{
	name = "";
	position[] = {7061.19,7718.04};
	type = "CityCenter";
	neighbors[] = {"ACityC_Guglovo","ACityC_StarySobor","ACityC_Gorka","ACityC_Mogilevka","ACityC_Grishino"};
	demography[] = {"CIV",0.3,"CIV_RU",0.7};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Guglovo
{
	name = "";
	position[] = {8466.55,6662.45};
	type = "CityCenter";
	neighbors[] = {"ACityC_NovySobor","ACityC_Staroye","ACityC_Shakhovka"};
	demography[] = {"CIV",0.5,"CIV_RU",0.5};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Vyshnoye
{
	name = "";
	position[] = {6565.11,6104.91};
	type = "CityCenter";
	neighbors[] = {"ACityC_Rogovo","ACityC_StarySobor","ACityC_Mogilevka"};
	demography[] = {"CIV",0.9,"CIV_RU",0.1};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Mogilevka
{
	name = "";
	position[] = {7576.98,5177.14};
	type = "CityCenter";
	neighbors[] = {"ACityC_Vyshnoye","ACityC_NovySobor","ACityC_Pusta","ACityC_Chernogorsk"};
	demography[] = {"CIV",0.8,"CIV_RU",0.2};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Nadezhdino
{
	name = "";
	position[] = {5871.47,4774.94};
	type = "CityCenter";
	neighbors[] = {"ACityC_Kozlovka","ACityC_Rogovo","ACityC_Chernogorsk"};
	demography[] = {"CIV",0.9,"CIV_RU",0.1};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Prigorodki
{
	name = "";
	position[] = {7982.71,3382.4};
	type = "CityCenter";
	neighbors[] = {"ACityC_Chernogorsk","ACityC_Elektrozavodsk"};
	demography[] = {"CIV",0.9,"CIV_RU",0.1};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Elektrozavodsk
{
	name = "";
	position[] = {10436.25,2138.39};
	type = "CityCenter";
	neighbors[] = {"ACityC_Pusta","ACityC_Prigorodki","ACityC_Kamyshovo","ACityC_Staroye"};
	demography[] = {"CIV",0.9,"CIV_RU",0.1};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Pusta
{
	name = "";
	position[] = {9169.62,3857.71};
	type = "CityCenter";
	neighbors[] = {"ACityC_Mogilevka","ACityC_Elektrozavodsk"};
	demography[] = {"CIV",0.9,"CIV_RU",0.1};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Staroye
{
	name = "";
	position[] = {10167.05,5572.38};
	type = "CityCenter";
	neighbors[] = {"ACityC_Shakhovka","ACityC_Guglovo","ACityC_Msta","ACityC_Elektrozavodsk","","ACityC_Dolina"};
	demography[] = {"CIV",0.8,"CIV_RU",0.2};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Msta
{
	name = "";
	position[] = {11273.13,5471.18};
	type = "CityCenter";
	neighbors[] = {"ACityC_Staroye","ACityC_Dolina","ACityC_Tulga"};
	demography[] = {"CIV",0.4,"CIV_RU",0.6};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Tulga
{
	name = "";
	position[] = {12775.79,4416.77};
	type = "CityCenter";
	neighbors[] = {"ACityC_Kamyshovo","ACityC_Solnychniy","ACityC_Msta"};
	demography[] = {"CIV",0.6,"CIV_RU",0.4};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Kamyshovo
{
	name = "";
	position[] = {12074.99,3583.77};
	type = "CityCenter";
	neighbors[] = {"ACityC_Solnychniy","ACityC_Tulga","ACityC_Elektrozavodsk"};
	demography[] = {"CIV",0.8,"CIV_RU",0.2};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Solnychniy
{
	name = "";
	position[] = {13328.86,6255.62};
	type = "CityCenter";
	neighbors[] = {"ACityC_Dolina","ACityC_Nizhnoye","ACityC_Tulga","ACityC_Kamyshovo"};
	demography[] = {"CIV",0.6,"CIV_RU",0.4};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Dolina
{
	name = "";
	position[] = {11281.46,6581.77};
	type = "CityCenter";
	neighbors[] = {"ACityC_Polana","ACityC_Shakhovka","ACityC_Staroye","ACityC_Solnychniy","ACityC_Msta"};
	demography[] = {"CIV",1,"CIV_RU",0};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Shakhovka
{
	name = "";
	position[] = {9624.39,6565.69};
	type = "CityCenter";
	neighbors[] = {"ACityC_Dolina","ACityC_Guglovo","ACityC_Polana","ACityC_Staroye"};
	demography[] = {"CIV",0.6,"CIV_RU",0.4};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Orlovets
{
	name = "";
	position[] = {12155.89,7266.46};
	type = "CityCenter";
	neighbors[] = {"ACityC_Polana","ACityC_Berezino"};
	demography[] = {"CIV",0.1,"CIV_RU",0.9};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Polana
{
	name = "";
	position[] = {10674.22,8033.22};
	type = "CityCenter";
	neighbors[] = {"ACityC_Dolina","ACityC_Shakhovka","ACityC_Orlovets","ACityC_Gorka","ACityC_Dubrovka"};
	demography[] = {"CIV",0.1,"CIV_RU",0.9};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Gorka
{
	name = "";
	position[] = {9598.56,8833.33};
	type = "CityCenter";
	neighbors[] = {"ACityC_Polana","ACityC_Dubrovka","ACityC_NovySobor","ACityC_Grishino","ACityC_Gvozdno"};
	demography[] = {"CIV",0.8,"CIV_RU",0.2};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Berezino
{
	name = "";
	position[] = {12480.4,9650.34};
	type = "CityCenter";
	neighbors[] = {"ACityC_Dubrovka","ACityC_Orlovets","ACityC_Nizhnoye","ACityC_Khelm"};
	demography[] = {"CIV",1,"CIV_RU",0};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Dubrovka
{
	name = "";
	position[] = {10430.28,9799.58};
	type = "CityCenter";
	neighbors[] = {"ACityC_Gorka","ACityC_Berezino","ACityC_Krasnostav","ACityC_Gvozdno","ACityC_Polana"};
	demography[] = {"CIV",0.1,"CIV_RU",0.9};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Gvozdno
{
	name = "";
	position[] = {8573.37,11948.01};
	type = "CityCenter";
	neighbors[] = {"ACityC_Gorka","ACityC_Krasnostav","ACityC_Dubrovka","ACityC_Grishino"};
	demography[] = {"CIV",0,"CIV_RU",1};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Krasnostav
{
	name = "";
	position[] = {11175.19,12282.79};
	type = "CityCenter";
	neighbors[] = {"ACityC_Olsha","ACityC_Dubrovka","ACityC_Gvozdno","ACityC_Khelm"};
	demography[] = {"CIV",0,"CIV_RU",1};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Olsha
{
	name = "";
	position[] = {13359.67,12873.09};
	type = "CityCenter";
	neighbors[] = {"ACityC_Krasnostav"};
	demography[] = {"CIV",0,"CIV_RU",1};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Khelm
{
	name = "";
	position[] = {12302.73,10796.32};
	type = "CityCenter";
	neighbors[] = {"ACityC_Berezino","ACityC_Krasnostav"};
	demography[] = {"CIV",0,"CIV_RU",1};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class ACityC_Nizhnoye
{
	name = "";
	position[] = {12945.77,8169.6};
	type = "CityCenter";
	neighbors[] = {"ACityC_Berezino","ACityC_Solnychniy"};
	demography[] = {"CIV",0,"CIV_RU",1};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlatC_Balota
{
	name = "";
	position[] = {4388.25,2538.51};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 30;
};
class AFlatC_Komarovo
{
	name = "";
	position[] = {3760.11,2473.73};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 150;
};
class AFlatC_Kamenka
{
	name = "";
	position[] = {1684.68,2234.78};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 20;
};
class AFlatC_Pavlovo
{
	name = "";
	position[] = {1732.25,3740.96};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 100;
};
class AFlatC_Bor
{
	name = "";
	position[] = {3481.35,3997.94};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 25;
};
class AFlatC_Kozlovka
{
	name = "";
	position[] = {4465.96,4570.83};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 270;
};
class AFlatC_Drozhino
{
	name = "";
	position[] = {3406.36,4872.27};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 220;
};
class AFlatC_Zelenogorsk
{
	name = "";
	position[] = {2671.15,5320.51};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 280;
};
class AFlatC_Sosnovka
{
	name = "";
	position[] = {2399.68,6303.76};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 205;
};
class AFlatC_Pulkovo
{
	name = "";
	position[] = {4892.73,5712.45};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 65;
};
class AFlatC_Nadezhdino
{
	name = "";
	position[] = {5807.61,4998.41};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 240;
};
class AFlatC_Pogorevka
{
	name = "";
	position[] = {4519.03,6324.5};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 360;
};
class AFlatC_Rogovo
{
	name = "";
	position[] = {4811.73,6728.05};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 355;
};
class AFlatC_Vyshnoye
{
	name = "";
	position[] = {6522.85,6149.59};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 50;
};
class AFlatC_NovySobor
{
	name = "";
	position[] = {7015.66,7645.47};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 160;
};
class AFlatC_StarySobor
{
	name = "";
	position[] = {6306.92,7540};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 210;
};
class AFlatC_Kabanino
{
	name = "";
	position[] = {5449.29,8608.54};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 265;
};
class AFlatC_Vybor
{
	name = "";
	position[] = {3635.42,8897.64};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 225;
};
class AFlatC_Pustoshka
{
	name = "";
	position[] = {2934.61,7780.66};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 275;
};
class AFlatC_Myshkino
{
	name = "";
	position[] = {1916.1,7439.7};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 250;
};
class AFlatC_Lopatino
{
	name = "";
	position[] = {2636.01,10089.05};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 235;
};
class AFlatC_Grishino
{
	name = "";
	position[] = {6008.88,10420.35};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 300;
};
class AFlatC_Petrovka
{
	name = "";
	position[] = {4977.83,12583.05};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 75;
};
class AFlatC_Elektrozavodsk
{
	name = "";
	position[] = {10248.83,2163.76};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 345;
};
class AFlatC_Prigorodki
{
	name = "";
	position[] = {7831.21,3340.26};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlatC_Kamyshovo
{
	name = "";
	position[] = {12300.64,3541.93};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 350;
};
class AFlatC_Pusta
{
	name = "";
	position[] = {9081.88,3753.14};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 85;
};
class AFlatC_Mogilevka
{
	name = "";
	position[] = {7641.62,4966.96};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 35;
};
class AFlatC_Tulga
{
	name = "";
	position[] = {12593.87,4356.49};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 195;
};
class AFlatC_Msta
{
	name = "";
	position[] = {11387.19,5503.74};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 10;
};
class AFlatC_Staroye
{
	name = "";
	position[] = {10278.82,5539.12};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 235;
};
class AFlatC_Shakhovka
{
	name = "";
	position[] = {9708.31,6479.32};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 300;
};
class AFlatC_Dolina
{
	name = "";
	position[] = {11442.92,6555.93};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 160;
};
class AFlatC_Orlovets
{
	name = "";
	position[] = {12166.42,7150};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 175;
};
class AFlatC_Solnychniy
{
	name = "";
	position[] = {13459.28,6064.03};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 95;
};
class AFlatC_Guglovo
{
	name = "";
	position[] = {8428.35,6800.75};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 15;
};
class AFlatC_Polana
{
	name = "";
	position[] = {10911.23,8136.78};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 90;
};
class AFlatC_Berezino
{
	name = "";
	position[] = {12181.83,9397.31};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 135;
};
class AFlatC_Gorka
{
	name = "";
	position[] = {9810.16,8776.46};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 185;
};
class AFlatC_Dubrovka
{
	name = "";
	position[] = {10483.02,9836.03};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 145;
};
class AFlatC_Khelm
{
	name = "";
	position[] = {12260.59,10782.73};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 270;
};
class AFlatC_Olsha
{
	name = "";
	position[] = {13387.81,12858.19};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 90;
};
class AFlatC_Krasnostav
{
	name = "";
	position[] = {10982.87,12377.36};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 195;
};
class AFlatC_Gvozdno
{
	name = "";
	position[] = {8510.66,12007.61};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 225;
};
class AFlatC_Nizhnoye
{
	name = "";
	position[] = {13034.06,8336.34};
	type = "FlatAreaCity";
	radiusA = 100;
	radiusB = 100;
	angle = 75;
};
class AFlatCS_Balota_02
{
	name = "";
	position[] = {4384.55,2380.82};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 35;
};
class AFlatCS_Komarovo_01
{
	name = "";
	position[] = {3531.16,2415.7};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 160;
};
class AFlatCS_Komarovo_02
{
	name = "";
	position[] = {3669.81,2412.33};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 95;
};
class AFlatCS_Kamenka_01
{
	name = "";
	position[] = {1835.86,2275.42};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 195;
};
class AFlatCS_Pavlovo_01
{
	name = "";
	position[] = {1637.99,3942};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 270;
};
class AFlatCS_Bor_01
{
	name = "";
	position[] = {3235.45,4011.75};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 25;
};
class AFlatCS_Nadezhdino_01
{
	name = "";
	position[] = {5868.47,4806.13};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 120;
};
class AFlatCS_Kozlovka_01
{
	name = "";
	position[] = {4301.77,4751.35};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 240;
};
class AFlatCS_Kozlovka_02
{
	name = "";
	position[] = {4401.23,4666.25};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 180;
};
class AFlatCS_Kozlovka_03
{
	name = "";
	position[] = {4469.24,4595.37};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 260;
};
class AFlatCS_Drozhino_01
{
	name = "";
	position[] = {3323.82,5002.93};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 215;
};
class AFlatCS_Drozhino_02
{
	name = "";
	position[] = {3487.01,4892.45};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 205;
};
class AFlatCS_Zelenogorsk_01
{
	name = "";
	position[] = {2544.68,5045.13};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 320;
};
class AFlatCS_Zelenogorsk_02
{
	name = "";
	position[] = {2752.29,5113.42};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 105;
};
class AFlatCS_Zelenogorsk_03
{
	name = "";
	position[] = {2692.45,5427.91};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 200;
};
class AFlatCS_Zelenogorsk_04
{
	name = "";
	position[] = {2878.56,5370.32};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 120;
};
class AFlatCS_Pulkovo_01
{
	name = "";
	position[] = {4856.25,5629.71};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 195;
};
class AFlatCS_Pogorevka_01
{
	name = "";
	position[] = {4346.98,6463.28};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 220;
};
class AFlatCS_Rogovo_01
{
	name = "";
	position[] = {4619.03,6863.36};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 205;
};
class AFlatCS_Vyshnoye_01
{
	name = "";
	position[] = {6422.01,6170.91};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 205;
};
class AFlatCS_Vyshnoye_02
{
	name = "";
	position[] = {6502.49,6108.76};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 240;
};
class AFlatCS_Sosnovka_01
{
	name = "";
	position[] = {2567.8,6290.77};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 150;
};
class AFlatCS_Sosnovka_02
{
	name = "";
	position[] = {2502.94,6460.31};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 270;
};
class AFlatCS_Myshkino_01
{
	name = "";
	position[] = {2123.76,7321.47};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 30;
};
class AFlatCS_Pustoshka_01
{
	name = "";
	position[] = {3158.45,8044.45};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 30;
};
class AFlatCS_Pustoshka_02
{
	name = "";
	position[] = {3140.71,8101.63};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 10;
	angle = 325;
};
class AFlatCS_NovySobor_01
{
	name = "";
	position[] = {7147.77,7573.71};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 95;
};
class AFlatCS_StarySobor_01
{
	name = "";
	position[] = {6226.96,7753.01};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 125;
};
class AFlatCS_Kabanino_01
{
	name = "";
	position[] = {5348.69,8731.31};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 200;
};
class AFlatCS_Vybor_01
{
	name = "";
	position[] = {3903.04,8799.05};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 200;
};
class AFlatCS_Vybor_02
{
	name = "";
	position[] = {3752.26,8639.15};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 125;
};
class AFlatCS_Lopatino_01
{
	name = "";
	position[] = {2788.91,9833.8};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 210;
};
class AFlatCS_Lopatino_02
{
	name = "";
	position[] = {2620.33,9880.63};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 300;
};
class AFlatCS_Lopatino_03
{
	name = "";
	position[] = {2710.26,10132.79};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 50;
};
class AFlatCS_Grishino_01
{
	name = "";
	position[] = {5834.99,10170.37};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 290;
};
class AFlatCS_Grishino_02
{
	name = "";
	position[] = {5912.03,10272.73};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 300;
};
class AFlatCS_Grishino_03
{
	name = "";
	position[] = {6043.37,10404.7};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 120;
};
class AFlatCS_Grishino_04
{
	name = "";
	position[] = {6039.71,10525.34};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 295;
};
class AFlatCS_Petrovka_01
{
	name = "";
	position[] = {5032.65,12449.92};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 210;
};
class AFlatCS_Krasnostav_01
{
	name = "";
	position[] = {11336.41,12276.48};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 115;
};
class AFlatCS_Krasnostav_02
{
	name = "";
	position[] = {11248.36,12170.7};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 30;
};
class AFlatCS_Krasnostav_03
{
	name = "";
	position[] = {11551.79,12250.48};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 355;
};
class AFlatCS_Krasnostav_04
{
	name = "";
	position[] = {11162.05,12140.07};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 290;
};
class AFlatCS_Krasnostav_05
{
	name = "";
	position[] = {10738.65,12407.92};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 165;
};
class AFlatCS_Gvozdno_01
{
	name = "";
	position[] = {8601.28,11857.93};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 235;
};
class AFlatCS_Gvozdno_02
{
	name = "";
	position[] = {8773.88,11697.92};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 80;
};
class AFlatCS_Gvozdno_03
{
	name = "";
	position[] = {8435.86,12008.55};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 180;
};
class AFlatCS_Gvozdno_04
{
	name = "";
	position[] = {8691,11841.14};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 45;
};
class AFlatCS_Khelm_01
{
	name = "";
	position[] = {12319.74,10729.6};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 100;
};
class AFlatCS_Khelm_02
{
	name = "";
	position[] = {12378.86,10792.62};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 90;
};
class AFlatCS_Dubrovka_01
{
	name = "";
	position[] = {10459.17,9967.9};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 270;
};
class AFlatCS_Dubrovka_02
{
	name = "";
	position[] = {10557.03,9597.84};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 195;
};
class AFlatCS_Dubrovka_03
{
	name = "";
	position[] = {10273.53,9622.66};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 245;
};
class AFlatCS_Berezino_01
{
	name = "";
	position[] = {11785.52,9224.77};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 350;
};
class AFlatCS_Berezino_02
{
	name = "";
	position[] = {11758.2,9119.88};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 235;
};
class AFlatCS_Berezino_03
{
	name = "";
	position[] = {12020.6,8912.44};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 145;
};
class AFlatCS_Berezino_04
{
	name = "";
	position[] = {12136.47,9071.39};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 20;
};
class AFlatCS_Berezino_05
{
	name = "";
	position[] = {12291.11,9049.66};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 60;
};
class AFlatCS_Berezino_06
{
	name = "";
	position[] = {12058.92,9258.5};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 120;
};
class AFlatCS_Berezino_07
{
	name = "";
	position[] = {12221.38,9564.58};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 15;
};
class AFlatCS_Berezino_08
{
	name = "";
	position[] = {12371.83,9652.38};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 120;
};
class AFlatCS_Berezino_09
{
	name = "";
	position[] = {12658.72,9792.38};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 330;
};
class AFlatCS_Berezino_10
{
	name = "";
	position[] = {12915.91,10067.47};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 120;
};
class AFlatCS_Berezino_11
{
	name = "";
	position[] = {12953.17,10243};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 290;
};
class AFlatCS_Berezino_12
{
	name = "";
	position[] = {12943.4,9826.84};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 285;
};
class AFlatCS_Gorka_01
{
	name = "";
	position[] = {9809.33,8822.68};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 5;
};
class AFlatCS_Gorka_02
{
	name = "";
	position[] = {9225.92,8787.83};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 205;
};
class AFlatCS_Gorka_03
{
	name = "";
	position[] = {9319.5,8929.39};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 325;
};
class AFlatCS_Nizhnoye_01
{
	name = "";
	position[] = {12964.75,8347.79};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 250;
};
class AFlatCS_Nizhnoye_02
{
	name = "";
	position[] = {12990.79,8245.67};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 255;
};
class AFlatCS_Nizhnoye_03
{
	name = "";
	position[] = {13056,8061.94};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 85;
};
class AFlatCS_Polana_01
{
	name = "";
	position[] = {10705.05,7982.89};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 210;
};
class AFlatCS_Polana_02
{
	name = "";
	position[] = {10773.8,8212.94};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 215;
};
class AFlatCS_Orlovets_01
{
	name = "";
	position[] = {12331.04,7402.01};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 305;
};
class AFlatCS_Orlovets_02
{
	name = "";
	position[] = {11988.74,7241.98};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 190;
};
class AFlatCS_Dolina_01
{
	name = "";
	position[] = {11127.16,6622.73};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 200;
};
class AFlatCS_Dolina_02
{
	name = "";
	position[] = {11360.85,6583.02};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlatCS_Shakhovka_01
{
	name = "";
	position[] = {9667.99,6449.84};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 75;
};
class AFlatCS_Shakhovka_02
{
	name = "";
	position[] = {9584.25,6641.26};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 75;
};
class AFlatCS_Guglovo_01
{
	name = "";
	position[] = {8343.99,6766.49};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 190;
};
class AFlatCS_Staroye_01
{
	name = "";
	position[] = {10295.51,5578.51};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 65;
};
class AFlatCS_Staroye_02
{
	name = "";
	position[] = {10111.32,5550.66};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 75;
};
class AFlatCS_Staroye_03
{
	name = "";
	position[] = {10020.67,5705.52};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 60;
};
class AFlatCS_Staroye_04
{
	name = "";
	position[] = {10123.77,5377.05};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 270;
};
class AFlatCS_Msta_01
{
	name = "";
	position[] = {11431.67,5564.05};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 35;
};
class AFlatCS_Msta_02
{
	name = "";
	position[] = {11124.69,5310.37};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 115;
};
class AFlatCS_Solnychniy_01
{
	name = "";
	position[] = {13163.82,6249.84};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AFlatCS_Solnychniy_02
{
	name = "";
	position[] = {13443.54,5939.22};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 100;
};
class AFlatCS_Solnychniy_03
{
	name = "";
	position[] = {13438.49,6411.26};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 270;
};
class AFlatCS_Prigorodki_01
{
	name = "";
	position[] = {7793.31,3496.96};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 165;
};
class AFlatCS_Mogilevka_01
{
	name = "";
	position[] = {7512.98,5054.14};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 295;
};
class AFlatCS_Mogilevka_02
{
	name = "";
	position[] = {7529.12,5291.37};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 245;
};
class AFlatCS_Pusta_01
{
	name = "";
	position[] = {9180.62,3928.19};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 325;
};
class AFlatCS_Elektrozavodsk_01
{
	name = "";
	position[] = {9650.88,2000.21};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 185;
};
class AFlatCS_Elektrozavodsk_02
{
	name = "";
	position[] = {10089.74,2114.46};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 350;
};
class AFlatCS_Elektrozavodsk_03
{
	name = "";
	position[] = {10321.7,1953.92};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 270;
};
class AFlatCS_Elektrozavodsk_04
{
	name = "";
	position[] = {10369.69,2048.25};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 125;
};
class AFlatCS_Elektrozavodsk_05
{
	name = "";
	position[] = {10511.66,2154.33};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 335;
};
class AFlatCS_Elektrozavodsk_06
{
	name = "";
	position[] = {10393.02,2265.15};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 305;
};
class AFlatCS_Elektrozavodsk_07
{
	name = "";
	position[] = {10512.11,2024.9};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 60;
};
class AFlatCS_Elektrozavodsk_08
{
	name = "";
	position[] = {10391.92,2516.4};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 5;
};
class AFlatCS_Elektrozavodsk_09
{
	name = "";
	position[] = {10685.31,2469.69};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 325;
};
class AFlatCS_Kamyshovo_01
{
	name = "";
	position[] = {11960.74,3515.92};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 355;
};
class AFlatCS_Kamyshovo_02
{
	name = "";
	position[] = {12253.18,3494.5};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 175;
};
class AFlatCS_Tulga_01
{
	name = "";
	position[] = {12566.93,4402.68};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 210;
};
class AFlatCS_Tulga_02
{
	name = "";
	position[] = {12791.64,4466.95};
	type = "FlatAreaCitySmall";
	radiusA = 100;
	radiusB = 100;
	angle = 5;
};
class AAirport_1
{
	name = "";
	position[] = {4783.72,2564.25};
	type = "Airport";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AAirport_2
{
	name = "";
	position[] = {4828.25,10205.31};
	type = "Airport";
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};
class AStrong_Sosnovka_03
{
	name = "";
	position[] = {2497.28,6803.58};
	type = "StrongpointArea";
	radiusA = 207.58;
	radiusB = 177.44;
	angle = 40;
};
class AFlatCS_StarySobor_02
{
	name = "";
	position[] = {6212.46,7805};
	type = "FlatAreaCitySmall";
	radiusA = 219.4;
	radiusB = 189.26;
	angle = 310;
};
class AFlatCS_NovySobor_02
{
	name = "";
	position[] = {7241.92,7646.02};
	type = "FlatAreaCitySmall";
	radiusA = 155.62;
	radiusB = 133.16;
	angle = 195;
};
class AFlatCS_NovySobor_03
{
	name = "";
	position[] = {7044.93,7609.06};
	type = "FlatAreaCitySmall";
	radiusA = 135.15;
	radiusB = 115.94;
	angle = 175;
};
class hill_Lysina
{
	name = "$STR_LOCATION_MT_LYSINA";
	position[] = {2655.95,14170.7};
	type = "Hill";
	radiusA = 125.34;
	radiusB = 141.67;
	angle = 0;
};
class hill_Misty_Peak
{
	name = "$STR_LOCATION_MT_MISTYPEAK";
	position[] = {3727.95,14491.6};
	type = "Hill";
	radiusA = 188.63;
	radiusB = 214.21;
	angle = 0;
};
class hill_Bashnya
{
	name = "$STR_LOCATION_MT_BASHNYA";
	position[] = {4178.27,11771.22};
	type = "Hill";
	radiusA = 276.97;
	radiusB = 315.47;
	angle = 0;
};
class hill_Veresnik
{
	name = "$STR_LOCATION_MT_VERESNIK";
	position[] = {4440.17,8070.54};
	type = "Hill";
	radiusA = 85.14;
	radiusB = 95.59;
	angle = 0;
};
class hill_Kurgan
{
	name = "$STR_LOCATION_MT_KURGAN";
	position[] = {3368.52,5296.87};
	type = "Hill";
	radiusA = 125.64;
	radiusB = 142.01;
	angle = 0;
};
class hill_Kustryk
{
	name = "$STR_LOCATION_MT_KUSTRYK";
	position[] = {4912.88,5063.45};
	type = "Hill";
	radiusA = 232.9;
	radiusB = 264.95;
	angle = 0;
};
class hill_Windy_Mountain
{
	name = "$STR_LOCATION_MT_WINDYMT";
	position[] = {3892.74,4200.59};
	type = "Hill";
	radiusA = 338.77;
	radiusB = 386.3;
	angle = 0;
};
class hill_Little_Hill
{
	name = "$STR_LOCATION_MT_LITTLEHILL";
	position[] = {6903.78,4919.66};
	type = "Hill";
	radiusA = 391.98;
	radiusB = 447.29;
	angle = 0;
};
class hill_Pop_Ivan
{
	name = "$STR_LOCATION_MT_POPIVAN";
	position[] = {6420.26,6570.66};
	type = "Hill";
	radiusA = 157.1;
	radiusB = 178.07;
	angle = 0;
};
class hill_Ridge_Lesnoy
{
	name = "$STR_LOCATION_MT_RIDGELESNOY";
	position[] = {8122.77,7815.54};
	type = "Hill";
	radiusA = 287.79;
	radiusB = 327.87;
	angle = 0;
};
class hill_Vysoky_Kamen
{
	name = "$STR_LOCATION_MT_VYSOKYKAMEN";
	position[] = {8940.19,4380.52};
	type = "Hill";
	radiusA = 279.91;
	radiusB = 318.84;
	angle = 0;
};
class hill_Dobryy
{
	name = "$STR_LOCATION_MT_DOBRYY";
	position[] = {10552.9,3061.03};
	type = "Hill";
	radiusA = 149.99;
	radiusB = 169.92;
	angle = 0;
};
class hill_Baranchik
{
	name = "$STR_LOCATION_MT_BARANCHIK";
	position[] = {10104.67,6202.21};
	type = "Hill";
	radiusA = 263.06;
	radiusB = 296.32;
	angle = 0;
};
class hill_Malinovka
{
	name = "$STR_LOCATION_MT_MALINOVKA";
	position[] = {10897.7,7575.59};
	type = "Hill";
	radiusA = 472.68;
	radiusB = 539.79;
	angle = 0;
};
class hill_Dubina
{
	name = "$STR_LOCATION_MT_DUBINA";
	position[] = {11107.9,8474.83};
	type = "Hill";
	radiusA = 351.49;
	radiusB = 400.89;
	angle = 0;
};
class hill_Klen
{
	name = "$STR_LOCATION_MT_KLEN";
	position[] = {11544,11381.1};
	type = "Hill";
	radiusA = 510.49;
	radiusB = 582.23;
	angle = 0;
};
class hill_Black_Mountain
{
	name = "$STR_LOCATION_MT_BLACKMT";
	position[] = {10277.63,12024.57};
	type = "Hill";
	radiusA = 469.73;
	radiusB = 536.41;
	angle = 0;
};
class hill_Ostry
{
	name = "$STR_LOCATION_MT_OSTRY";
	position[] = {10807.6,12848.4};
	type = "Hill";
	radiusA = 349.39;
	radiusB = 398.47;
	angle = 0;
};
class hill_Olsha
{
	name = "$STR_LOCATION_MT_OLSHA";
	position[] = {12975.7,12775.2};
	type = "Hill";
	radiusA = 141.52;
	radiusB = 160.21;
	angle = 0;
};
class w_Tikhaya
{
	name = "$STR_LOCATION_W_BAYTIKHAYA";
	position[] = {1221.49,2111.89};
	type = "NameMarine";
	radiusA = 255.79;
	radiusB = 291.19;
	angle = 0;
};
class w_Mutnaya
{
	name = "$STR_LOCATION_W_BAYMUTNAYA";
	position[] = {5659.94,1919.19};
	type = "NameMarine";
	radiusA = 196.48;
	radiusB = 223.21;
	angle = 0;
};
class w_Chyornaya_Bay
{
	name = "$STR_LOCATION_W_BAYCHYORNAYA";
	position[] = {7620.49,3024.65};
	type = "NameMarine";
	radiusA = 130.67;
	radiusB = 147.77;
	angle = 0;
};
class w_Zelenaya
{
	name = "$STR_LOCATION_W_BAYZELENAYA";
	position[] = {11303.69,3060.39};
	type = "NameMarine";
	radiusA = 241.66;
	radiusB = 275;
	angle = 0;
};
class w_Skalisty_Proliv
{
	name = "$STR_LOCATION_W_SKALISTYPROLIV";
	position[] = {13385.92,3613.94};
	type = "NameMarine";
	radiusA = 186.38;
	radiusB = 211.64;
	angle = 0;
};
class w_Nizhnaya
{
	name = "$STR_LOCATION_W_BAYNIZHNAYA";
	position[] = {12989.3,8515.76};
	type = "NameMarine";
	radiusA = 180.18;
	radiusB = 204.52;
	angle = 0;
};
class w_Rify
{
	name = "$STR_LOCATION_W_RIFY";
	position[] = {13802.2,11185.61};
	type = "NameMarine";
	radiusA = 214.88;
	radiusB = 244.3;
	angle = 0;
};
class w_Willow_Lake
{
	name = "$STR_LOCATION_W_LAKEWILLOW";
	position[] = {13285.05,11560};
	type = "NameMarine";
	radiusA = 320.32;
	radiusB = 364.18;
	angle = 0;
};
class w_Black_Lake
{
	name = "$STR_LOCATION_W_LAKEBLACK";
	position[] = {13322.6,12050};
	type = "NameMarine";
	radiusA = 296.41;
	radiusB = 341.67;
	angle = 0;
};
class w_Pobeda_Dam
{
	name = "$STR_LOCATION_W_DAMPOBEDA";
	position[] = {9464.53,13623.58};
	type = "NameMarine";
	radiusA = 159.59;
	radiusB = 180.92;
	angle = 0;
};
class w_Topolka_Dam
{
	name = "$STR_LOCATION_W_DAMTOPOLKA";
	position[] = {10231.33,3691.15};
	type = "NameMarine";
	radiusA = 72.06;
	radiusB = 80.6;
	angle = 0;
};
class w_Ozerko
{
	name = "$STR_LOCATION_W_OZERKO";
	position[] = {6777.63,4492.42};
	type = "NameMarine";
	radiusA = 94.14;
	radiusB = 105.91;
	angle = 0;
};
class w_Prud
{
	name = "$STR_LOCATION_W_PRUD";
	position[] = {6566.98,9254.57};
	type = "NameMarine";
	radiusA = 67.5;
	radiusB = 75.36;
	angle = 0;
};
class w_Guba
{
	name = "$STR_LOCATION_W_GUBA";
	position[] = {14303.16,13162.99};
	type = "NameMarine";
	radiusA = 344.67;
	radiusB = 393.07;
	angle = 0;
};
class Local_Pass_Grozovoy
{
	name = "$STR_LOCATION_LOC_PASSGROZOVOY";
	position[] = {1451.82,14421.94};
	type = "NameLocal";
	radiusA = 427.43;
	radiusB = 487.92;
	angle = 0;
};
class Local_Pass_Sosnovy
{
	name = "$STR_LOCATION_LOC_PASSSOSNOVY";
	position[] = {2687.4,6590.28};
	type = "NameLocal";
	radiusA = 168.62;
	radiusB = 191.28;
	angle = 0;
};
class Local_Green_Mountain
{
	name = "$STR_LOCATION_LOC_GREENMOUNTAIN";
	position[] = {3767.17,6010.54};
	type = "NameLocal";
	radiusA = 256.14;
	radiusB = 290.56;
	angle = 0;
};
class Local_Airstrip1
{
	name = "$STR_LOCATION_AIRSTRIP";
	position[] = {4955.75,2427.69};
	type = "NameLocal";
	radiusA = 130.83;
	radiusB = 147.96;
	angle = 0;
};
class Local_Cement_Factory
{
	name = "$STR_LOCATION_FACTORY";
	position[] = {6515.46,2683.52};
	type = "NameLocal";
	radiusA = 107.17;
	radiusB = 120.84;
	angle = 0;
};
class Local_Vysota
{
	name = "$STR_LOCATION_LOC_VYSOTA";
	position[] = {6591.63,3400};
	type = "NameLocal";
	radiusA = 160.8;
	radiusB = 182.67;
	angle = 0;
};
class Local_Kopyto
{
	name = "$STR_LOCATION_LOC_KOPYTO";
	position[] = {7879.16,3904.42};
	type = "NameLocal";
	radiusA = 174.97;
	radiusB = 198.55;
	angle = 0;
};
class Local_Cap_Golova
{
	name = "$STR_LOCATION_LOC_CAPGOLOVA";
	position[] = {8322.08,2450};
	type = "NameLocal";
	radiusA = 370.61;
	radiusB = 422.8;
	angle = 0;
};
class Local_Drakon
{
	name = "$STR_LOCATION_LOC_DRAKON";
	position[] = {11160.51,2477.5};
	type = "NameLocal";
	radiusA = 85.14;
	radiusB = 95.59;
	angle = 0;
};
class Local_Otmel
{
	name = "$STR_LOCATION_LOC_OTMEL";
	position[] = {11581.25,3213.24};
	type = "NameLocal";
	radiusA = 199.36;
	radiusB = 226.51;
	angle = 0;
};
class Local_Skalisty_Island
{
	name = "$STR_LOCATION_LOC_ISLANDSKALISTY";
	position[] = {13728.9,2919.62};
	type = "NameLocal";
	radiusA = 153.26;
	radiusB = 173.67;
	angle = 0;
};
class Local_Cap_Krutnoy
{
	name = "$STR_LOCATION_LOC_CAPKRUTOY";
	position[] = {13578.19,3976.82};
	type = "NameLocal";
	radiusA = 124.21;
	radiusB = 140.37;
	angle = 0;
};
class Local_Three_Valleys
{
	name = "$STR_LOCATION_LOC_THREEVALLEYS";
	position[] = {12764.47,5412.21};
	type = "NameLocal";
	radiusA = 629.64;
	radiusB = 718.83;
	angle = 0;
};
class Local_Quarry
{
	name = "$STR_LOCATION_QUARRY";
	position[] = {13102.98,6072.1};
	type = "NameLocal";
	radiusA = 239.51;
	radiusB = 271.47;
	angle = 0;
};
class Local_Factory1
{
	name = "$STR_LOCATION_FACTORY";
	position[] = {13057.04,7120};
	type = "NameLocal";
	radiusA = 286.3;
	radiusB = 326.17;
	angle = 0;
};
class Local_Factory2
{
	name = "$STR_LOCATION_FACTORY";
	position[] = {11497.72,7490};
	type = "NameLocal";
	radiusA = 186.89;
	radiusB = 211.06;
	angle = 0;
};
class Local_Lumber_Mill
{
	name = "$STR_LOCATION_LUMBERMILL";
	position[] = {12676.66,9658.46};
	type = "NameLocal";
	radiusA = 68.38;
	radiusB = 76.38;
	angle = 0;
};
class Local_Blunt_Rocks
{
	name = "$STR_LOCATION_LOC_BLUNTROCKS";
	position[] = {13115.06,11900};
	type = "NameLocal";
	radiusA = 215.13;
	radiusB = 244.59;
	angle = 0;
};
class Local_Airstrip
{
	name = "$STR_LOCATION_AIRSTRIP";
	position[] = {12034.32,12788.44};
	type = "NameLocal";
	radiusA = 136.67;
	radiusB = 154.66;
	angle = 0;
};
class Local_Novy_Lug
{
	name = "$STR_LOCATION_LOC_NOVYLUG";
	position[] = {9491.63,11219.87};
	type = "NameLocal";
	radiusA = 166.29;
	radiusB = 188.61;
	angle = 0;
};
class Local_Black_Forest
{
	name = "$STR_LOCATION_LOC_BLACKFOREST";
	position[] = {9067.52,7600};
	type = "NameLocal";
	radiusA = 231.78;
	radiusB = 263.67;
	angle = 0;
};
class Local_Pass_Oreshka
{
	name = "$STR_LOCATION_LOC_PASSORESHKA";
	position[] = {9935.03,6044.51};
	type = "NameLocal";
	radiusA = 146.19;
	radiusB = 165.57;
	angle = 0;
};
class Local_Kumyrna
{
	name = "$STR_LOCATION_LOC_KUMYRNA";
	position[] = {8288.55,6058.34};
	type = "NameLocal";
	radiusA = 97.25;
	radiusB = 109.47;
	angle = 0;
};
class Local_Old_Fields
{
	name = "$STR_LOCATION_LOC_OLDFIELDS";
	position[] = {6847.5,9219.06};
	type = "NameLocal";
	radiusA = 149.42;
	radiusB = 169.27;
	angle = 0;
};
class Local_Airfield
{
	name = "$STR_LOCATION_AIRFIELD";
	position[] = {4631.52,10192.23};
	type = "NameLocal";
	radiusA = 171.47;
	radiusB = 193.34;
	angle = 0;
};
class V_fir
{
	name = "";
	position[] = {8906.13,5500};
	type = "VegetationFir";
	radiusA = 176.65;
	radiusB = 204.4;
	angle = 0;
};
class V_broad
{
	name = "";
	position[] = {8212.63,3782.34};
	type = "VegetationBroadleaf";
	radiusA = 171.48;
	radiusB = 194.55;
	angle = 0;
};
class V_broad1
{
	name = "";
	position[] = {10543.01,2992.66};
	type = "VegetationBroadleaf";
	radiusA = 136.22;
	radiusB = 154.14;
	angle = 0;
};
class V_broad2
{
	name = "";
	position[] = {12739.67,4638.12};
	type = "VegetationBroadleaf";
	radiusA = 135.37;
	radiusB = 153.16;
	angle = 0;
};
class V_broad3
{
	name = "";
	position[] = {12826.02,7534.79};
	type = "VegetationBroadleaf";
	radiusA = 134.6;
	radiusB = 152.28;
	angle = 0;
};
class V_broad4
{
	name = "";
	position[] = {11308.67,9942.01};
	type = "VegetationBroadleaf";
	radiusA = 133.92;
	radiusB = 151.5;
	angle = 0;
};
class V_broad5
{
	name = "";
	position[] = {11626.61,13559.44};
	type = "VegetationBroadleaf";
	radiusA = 163.21;
	radiusB = 185.08;
	angle = 0;
};
class V_broad6
{
	name = "";
	position[] = {2141.27,3363.08};
	type = "VegetationBroadleaf";
	radiusA = 159.48;
	radiusB = 180.8;
	angle = 0;
};
class V_broad7
{
	name = "";
	position[] = {1242.32,3161.05};
	type = "VegetationBroadleaf";
	radiusA = 236.55;
	radiusB = 268.85;
	angle = 0;
};
class V_broad8
{
	name = "";
	position[] = {9615.74,4585.6};
	type = "VegetationBroadleaf";
	radiusA = 149.09;
	radiusB = 168.71;
	angle = 0;
};
class V_broad9
{
	name = "";
	position[] = {5950.63,4072.75};
	type = "VegetationBroadleaf";
	radiusA = 222.03;
	radiusB = 252.23;
	angle = 0;
};
class V_broad10
{
	name = "";
	position[] = {6375.96,3988.7};
	type = "VegetationBroadleaf";
	radiusA = 330.67;
	radiusB = 375.63;
	angle = 0;
};
class V_broad11
{
	name = "";
	position[] = {5340.66,11380.01};
	type = "VegetationBroadleaf";
	radiusA = 165.11;
	radiusB = 187.05;
	angle = 0;
};
class V_broad12
{
	name = "";
	position[] = {9495.71,10822.4};
	type = "VegetationBroadleaf";
	radiusA = 161.35;
	radiusB = 182.75;
	angle = 0;
};
class V_broad13
{
	name = "";
	position[] = {10562.66,12070.5};
	type = "VegetationBroadleaf";
	radiusA = 194.07;
	radiusB = 220.21;
	angle = 0;
};
class V_broad15
{
	name = "";
	position[] = {1563.88,2593.79};
	type = "VegetationBroadleaf";
	radiusA = 187.26;
	radiusB = 212.41;
	angle = 0;
};
class V_broad14
{
	name = "";
	position[] = {1995.36,2390.72};
	type = "VegetationBroadleaf";
	radiusA = 181.17;
	radiusB = 205.44;
	angle = 0;
};
class V_broad16
{
	name = "";
	position[] = {2453.14,2741.07};
	type = "VegetationBroadleaf";
	radiusA = 216.23;
	radiusB = 245.58;
	angle = 0;
};
class V_broad17
{
	name = "";
	position[] = {2464.73,2269.39};
	type = "VegetationBroadleaf";
	radiusA = 207.08;
	radiusB = 235.1;
	angle = 0;
};
class V_broad18
{
	name = "";
	position[] = {6644.53,3307.91};
	type = "VegetationBroadleaf";
	radiusA = 161.85;
	radiusB = 183.32;
	angle = 0;
};
class V_fir1
{
	name = "";
	position[] = {8961.88,4721.06};
	type = "VegetationFir";
	radiusA = 211.17;
	radiusB = 239.79;
	angle = 0;
};
class V_fir2
{
	name = "";
	position[] = {10100.73,4908.68};
	type = "VegetationFir";
	radiusA = 202.56;
	radiusB = 229.93;
	angle = 0;
};
class V_fir3
{
	name = "";
	position[] = {10996.87,4539.14};
	type = "VegetationFir";
	radiusA = 194.85;
	radiusB = 221.1;
	angle = 0;
};
class V_fir4
{
	name = "";
	position[] = {11850.91,4173.95};
	type = "VegetationFir";
	radiusA = 153.1;
	radiusB = 173.3;
	angle = 0;
};
class V_fir5
{
	name = "";
	position[] = {12336.92,3801.76};
	type = "VegetationFir";
	radiusA = 234.96;
	radiusB = 265.96;
	angle = 0;
};
class V_fir7
{
	name = "";
	position[] = {12898.46,4065.38};
	type = "VegetationFir";
	radiusA = 360.57;
	radiusB = 408.15;
	angle = 0;
};
class V_fir8
{
	name = "";
	position[] = {13167.43,4309.48};
	type = "VegetationFir";
	radiusA = 220.08;
	radiusB = 249.99;
	angle = 0;
};
class V_fir9
{
	name = "";
	position[] = {11515.38,4184.56};
	type = "VegetationFir";
	radiusA = 171.15;
	radiusB = 193.97;
	angle = 0;
};
class V_fir10
{
	name = "";
	position[] = {12063.38,4914.45};
	type = "VegetationFir";
	radiusA = 259.97;
	radiusB = 294.63;
	angle = 0;
};
class V_fir11
{
	name = "";
	position[] = {12061.41,7804.22};
	type = "VegetationFir";
	radiusA = 162.49;
	radiusB = 184.06;
	angle = 0;
};
class V_fir12
{
	name = "";
	position[] = {11472.24,8657.76};
	type = "VegetationFir";
	radiusA = 159.01;
	radiusB = 180.07;
	angle = 0;
};
class V_fir13
{
	name = "";
	position[] = {11780.3,11645.96};
	type = "VegetationFir";
	radiusA = 155.9;
	radiusB = 176.51;
	angle = 0;
};
class V_fir14
{
	name = "";
	position[] = {10853.67,13219.08};
	type = "VegetationFir";
	radiusA = 231.56;
	radiusB = 263.13;
	angle = 0;
};
class V_fir15
{
	name = "";
	position[] = {10247.44,12874.77};
	type = "VegetationFir";
	radiusA = 343.92;
	radiusB = 390.82;
	angle = 0;
};
class V_fir16
{
	name = "";
	position[] = {7185.06,12000.32};
	type = "VegetationFir";
	radiusA = 171.18;
	radiusB = 194;
	angle = 0;
};
class V_fir17
{
	name = "";
	position[] = {6287.21,11720.81};
	type = "VegetationFir";
	radiusA = 205.06;
	radiusB = 232.79;
	angle = 0;
};
class V_fir18
{
	name = "";
	position[] = {3659.11,11561.82};
	type = "VegetationFir";
	radiusA = 307.05;
	radiusB = 348.58;
	angle = 0;
};
class V_fir19
{
	name = "";
	position[] = {2353.78,12590.91};
	type = "VegetationFir";
	radiusA = 233.38;
	radiusB = 265.22;
	angle = 0;
};
class V_fir20
{
	name = "";
	position[] = {1662.64,6534.22};
	type = "VegetationFir";
	radiusA = 274.61;
	radiusB = 312.42;
	angle = 0;
};
class V_broad19
{
	name = "";
	position[] = {1898.61,6035.53};
	type = "VegetationBroadleaf";
	radiusA = 403.88;
	radiusB = 459.49;
	angle = 0;
};
class V_broad20
{
	name = "";
	position[] = {1671.58,7014.02};
	type = "VegetationBroadleaf";
	radiusA = 244.87;
	radiusB = 278.37;
	angle = 0;
};
class V_fir21
{
	name = "";
	position[] = {6181.74,4665.88};
	type = "VegetationFir";
	radiusA = 188.89;
	radiusB = 214.28;
	angle = 0;
};
class V_fir22
{
	name = "";
	position[] = {6808.52,6225.47};
	type = "VegetationFir";
	radiusA = 182.63;
	radiusB = 207.11;
	angle = 0;
};
class V_fir23
{
	name = "";
	position[] = {6766.88,6820.21};
	type = "VegetationFir";
	radiusA = 177.02;
	radiusB = 200.69;
	angle = 0;
};
class V_fir24
{
	name = "";
	position[] = {8847.72,3573.4};
	type = "VegetationFir";
	radiusA = 140.35;
	radiusB = 158.7;
	angle = 0;
};
class V_fir25
{
	name = "";
	position[] = {9056.14,2851.6};
	type = "VegetationFir";
	radiusA = 139.21;
	radiusB = 157.4;
	angle = 0;
};
class V_fir26
{
	name = "";
	position[] = {10359.02,6426.5};
	type = "VegetationFir";
	radiusA = 138.19;
	radiusB = 156.23;
	angle = 0;
};
class V_fir27
{
	name = "";
	position[] = {9399.06,8022.21};
	type = "VegetationFir";
	radiusA = 137.28;
	radiusB = 155.19;
	angle = 0;
};
class V_fir28
{
	name = "";
	position[] = {8146.79,7870.69};
	type = "VegetationFir";
	radiusA = 213.09;
	radiusB = 240.89;
	angle = 0;
};
class V_fir29
{
	name = "";
	position[] = {10197.02,11847.42};
	type = "VegetationFir";
	radiusA = 204.27;
	radiusB = 231.88;
	angle = 0;
};
class V_fir30
{
	name = "";
	position[] = {8118.65,12492.17};
	type = "VegetationFir";
	radiusA = 130.61;
	radiusB = 147.55;
	angle = 0;
};
class V_fir31
{
	name = "";
	position[] = {7351.65,4198.65};
	type = "VegetationFir";
	radiusA = 184.27;
	radiusB = 208.99;
	angle = 0;
};
class V_fir32
{
	name = "";
	position[] = {5508.17,5208.87};
	type = "VegetationFir";
	radiusA = 119.16;
	radiusB = 134.45;
	angle = 0;
};
class V_fir33
{
	name = "";
	position[] = {5446.21,6182.29};
	type = "VegetationFir";
	radiusA = 188.13;
	radiusB = 212.26;
	angle = 0;
};
class V_broad21
{
	name = "";
	position[] = {5086.7,6132.5};
	type = "VegetationBroadleaf";
	radiusA = 188.89;
	radiusB = 214.28;
	angle = 0;
};
class V_fir34
{
	name = "";
	position[] = {4580.99,5151.71};
	type = "VegetationFir";
	radiusA = 148.84;
	radiusB = 168.42;
	angle = 0;
};
class V_fir35
{
	name = "";
	position[] = {3536.92,5865.87};
	type = "VegetationFir";
	radiusA = 221.68;
	radiusB = 251.82;
	angle = 0;
};
class V_fir36
{
	name = "";
	position[] = {3879.61,6059.51};
	type = "VegetationFir";
	radiusA = 172.3;
	radiusB = 195.29;
	angle = 0;
};
class V_fir37
{
	name = "";
	position[] = {3750.79,3444.96};
	type = "VegetationFir";
	radiusA = 206.31;
	radiusB = 234.23;
	angle = 0;
};
class V_fir38
{
	name = "";
	position[] = {3877.26,4151.61};
	type = "VegetationFir";
	radiusA = 198.21;
	radiusB = 224.95;
	angle = 0;
};
class V_fir39
{
	name = "";
	position[] = {1484.4,8440.14};
	type = "VegetationFir";
	radiusA = 190.96;
	radiusB = 216.65;
	angle = 0;
};
class V_fir40
{
	name = "";
	position[] = {6423.22,5522.86};
	type = "VegetationFir";
	radiusA = 150.32;
	radiusB = 170.12;
	angle = 0;
};
class V_broad22
{
	name = "";
	position[] = {5155.9,6557.89};
	type = "VegetationBroadleaf";
	radiusA = 290.84;
	radiusB = 329.09;
	angle = 0;
};