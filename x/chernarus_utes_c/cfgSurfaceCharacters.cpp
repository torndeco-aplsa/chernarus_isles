class CfgSurfaceCharacters
{
	class CRGrassClutter
	{
		probability[] = {0.79,0.1,0.1,0.01};
		names[] = {"GrassCrookedGreen","GrassCrooked","AutumnFlowers","WeedDead"};
	};
	class CRTallGrassClutter
	{
		probability[] = {0.4,0.2,0.3,0.07,0.02,0.01};
		names[] = {"GrassTall","AutumnFlowers","GrassBunch","GrassCrooked","WeedDead","WeedDeadSmall"};
	};
	class CRGrassWClutter
	{
		probability[] = {0.65,0.17,0.1,0.05,0.03};
		names[] = {"GrassCrooked","GrassCrookedGreen","AutumnFlowers","WeedDead","WeedDeadSmall"};
	};
	class CRTallGrassWClutter
	{
		probability[] = {0.3,0.25,0.2,0.2,0.03,0.02};
		names[] = {"GrassTall","AutumnFlowers","GrassBunch","GrassCrooked","WeedDead","WeedDeadSmall"};
	};
	class CRForestMixedClutter
	{
		probability[] = {0.2,0.1,0.2,0.001,0.003};
		names[] = {"GrassCrookedForest","FernAutumn","FernAutumnTall","MushroomsHorcak","MushroomsPrasivka"};
	};
	class CRForestFirClutter
	{
		probability[] = {0.4,0.1,0.1,0.15,0.05,0.003,0.005,0.008,0.004};
		names[] = {"BlueBerry","FernAutumn","FernAutumnTall","SmallPicea","RaspBerry","MushroomsHorcak","MushroomsPrasivka","MushroomsBabka","MushroomsMuchomurka"};
	};
	class CRHeatherClutter
	{
		probability[] = {0.15,0.5,0.3,0.1};
		names[] = {"BlueBerry","HeatherBrush","GrassCrooked","WeedSedge"};
	};
	class CRStubbleClutter
	{
		probability[] = {0.975,0.01,0.01,0.005};
		names[] = {"StubbleClutter","AutumnFlowers","WeedDeadSmall","WeedDead"};
	};
};
