class Utes_VillKamenyy {
	name = "$STR_LOCATION_KAMENYY";
	position[] = {12362.12, 2341.73};
	type = "NameVillage";
	speech[] = {"Kamenyy"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};

class Utes_VillStrelka {
	name = "$STR_LOCATION_STRELKA";
	position[] = {13402.12, 1099.73};
	type = "NameVillage";
	speech[] = {"Strelka"};
	radiusA = 100;
	radiusB = 100;
	angle = 0;
};

class Utes_FArea01 {
	name = "";
	position[] = {11644.12, 1781.73};
	type = "FlatArea";
	radiusA = 50;
	radiusB = 50;
	angle = 0;
};

class Utes_FArea02 {
	name = "";
	position[] = {12217.12, 2413.73};
	type = "FlatArea";
	radiusA = 50;
	radiusB = 50;
	angle = 0;
};

class Utes_FArea03 {
	name = "";
	position[] = {12772.12, 1792.73};
	type = "FlatArea";
	radiusA = 50;
	radiusB = 50;
	angle = 0;
};

class Utes_FArea04 {
	name = "";
	position[] = {13352.12, 1414.73};
	type = "FlatArea";
	radiusA = 50;
	radiusB = 50;
	angle = 0;
};

class Utes_FArea05 {
	name = "";
	position[] = {12552.12, 979.73};
	type = "FlatArea";
	radiusA = 50;
	radiusB = 50;
	angle = 0;
};

class Utes_SPArea011 {
	name = "";
	position[] = {12311.12, 2374.73};
	type = "StrongpointArea";
	radiusA = 30;
	radiusB = 30;
	angle = 0;
};

class Utes_FAreaC01 {
	name = "";
	position[] = {12557.12, 2304.73};
	type = "FlatAreaCity";
	radiusA = 40;
	radiusB = 40;
	angle = 1;
};

class Utes_FAreaCS101 {
	name = "";
	position[] = {12514.12, 2318.73};
	type = "FlatAreaCitySmall";
	radiusA = 20;
	radiusB = 20;
	angle = 240;
};

class Utes_SPArea012 {
	name = "";
	position[] = {12419.12, 2200.73};
	type = "StrongpointArea";
	radiusA = 30;
	radiusB = 30;
	angle = 170;
};

class Utes_SPArea013 {
	name = "";
	position[] = {12633.12, 2248.73};
	type = "StrongpointArea";
	radiusA = 30;
	radiusB = 30;
	angle = 120;
};

class Utes_FAreaC02 {
	name = "";
	position[] = {12206.12, 1897.73};
	type = "FlatAreaCity";
	radiusA = 40;
	radiusB = 40;
	angle = 120;
};

class Utes_FAreaCS201 {
	name = "";
	position[] = {12176.12, 1932.73};
	type = "FlatAreaCitySmall";
	radiusA = 20;
	radiusB = 20;
	angle = 260;
};

class Utes_SPArea021 {
	name = "";
	position[] = {12153.12, 1893.73};
	type = "StrongpointArea";
	radiusA = 30;
	radiusB = 30;
	angle = 200;
};

class Utes_FAreaCS102 {
	name = "";
	position[] = {12377.12, 2320.73};
	type = "FlatAreaCitySmall";
	radiusA = 20;
	radiusB = 20;
	angle = 320;
};

class Utes_SPArea022 {
	name = "";
	position[] = {12219.12, 1990.73};
	type = "StrongpointArea";
	radiusA = 30;
	radiusB = 30;
	angle = 300;
};

class Utes_SPArea023 {
	name = "";
	position[] = {12318.12, 1955.73};
	type = "StrongpointArea";
	radiusA = 30;
	radiusB = 30;
	angle = 80;
};

class Utes_SPArea031 {
	name = "";
	position[] = {12392.12, 1551.73};
	type = "StrongpointArea";
	radiusA = 30;
	radiusB = 30;
	angle = 340;
};

class Utes_FAreaC03 {
	name = "";
	position[] = {12524.12, 1549.73};
	type = "FlatAreaCity";
	radiusA = 40;
	radiusB = 40;
	angle = 0;
};

class Utes_FAreaCS301 {
	name = "";
	position[] = {12529.12, 1598.73};
	type = "FlatAreaCitySmall";
	radiusA = 20;
	radiusB = 20;
	angle = 350;
};

class Utes_SPArea041 {
	name = "";
	position[] = {12904.12, 1284.73};
	type = "StrongpointArea";
	radiusA = 30;
	radiusB = 30;
	angle = 100;
};

class Utes_FAreaC04 {
	name = "";
	position[] = {12840.12, 1217.73};
	type = "FlatAreaCity";
	radiusA = 40;
	radiusB = 40;
	angle = 230;
};

class Utes_FAreaCS401 {
	name = "";
	position[] = {12912.12, 1196.73};
	type = "FlatAreaCitySmall";
	radiusA = 20;
	radiusB = 20;
	angle = 200;
};

class Utes_SPArea051 {
	name = "";
	position[] = {13261.12, 1129.73};
	type = "StrongpointArea";
	radiusA = 30;
	radiusB = 30;
	angle = 280;
};

class Utes_FAreaC05 {
	name = "";
	position[] = {13376.12, 1197.73};
	type = "FlatAreaCity";
	radiusA = 40;
	radiusB = 40;
	angle = 200;
};

class Utes_FAreaCS501 {
	name = "";
	position[] = {13289.12, 1033.73};
	type = "FlatAreaCitySmall";
	radiusA = 20;
	radiusB = 20;
	angle = 170;
};

class Utes_FAreaCS302 {
	name = "";
	position[] = {12577.12, 1542.73};
	type = "FlatAreaCitySmall";
	radiusA = 20;
	radiusB = 20;
	angle = 0;
};

class Utes_SPArea032 {
	name = "";
	position[] = {12598.12, 1640.73};
	type = "StrongpointArea";
	radiusA = 30;
	radiusB = 30;
	angle = 0;
};

class Utes_SPArea033 {
	name = "";
	position[] = {12673.12, 1541.73};
	type = "StrongpointArea";
	radiusA = 30;
	radiusB = 30;
	angle = 90;
};

class Utes_SPArea034 {
	name = "";
	position[] = {12533.12, 1448.73};
	type = "StrongpointArea";
	radiusA = 30;
	radiusB = 30;
	angle = 230;
};

class Utes_SPArea042 {
	name = "";
	position[] = {12785.12, 1268.73};
	type = "StrongpointArea";
	radiusA = 30;
	radiusB = 30;
	angle = 240;
};

class Utes_SPArea043 {
	name = "";
	position[] = {12983.12, 1192.73};
	type = "StrongpointArea";
	radiusA = 30;
	radiusB = 30;
	angle = 200;
};

class Utes_SPArea052 {
	name = "";
	position[] = {13438.12, 1224.73};
	type = "StrongpointArea";
	radiusA = 30;
	radiusB = 30;
	angle = 30;
};

class Utes_SPArea053 {
	name = "";
	position[] = {13300.12, 991.73};
	type = "StrongpointArea";
	radiusA = 30;
	radiusB = 30;
	angle = 200;
};

class Utes_CC01 {
	name = "$STR_LOCATION_KAMENYY";
	position[] = {12409.12, 2295.73};
	type = "CityCenter";
	neighbors[] = {"Utes_CC02"};
	demography[] = {CIV, 0, CIV_RU, 1};
	radiusA = 10;
	radiusB = 10;
	angle = 0;
};

class Utes_CC02 {
	name = "$STR_LOCATION_MILITARY";
	position[] = {12231.12, 1928.73};
	type = "CityCenter";
	neighbors[] = {"Utes_CC01", "Utes_CC03"};
	demography[] = {CIV, 0, CIV_RU, 0};
	radiusA = 10;
	radiusB = 10;
	angle = 0;
};

class Utes_CC03 {
	name = "$STR_LOCATION_AIRFIELD";
	position[] = {12534.12, 1556.73};
	type = "CityCenter";
	neighbors[] = {"Utes_CC02", "Utes_CC04"};
	demography[] = {CIV, 0, CIV_RU, 0};
	radiusA = 10;
	radiusB = 10;
	angle = 0;
};

class Utes_CC04 {
	name = "$STR_DN_BARRACKS";
	position[] = {12880.12, 1220.73};
	type = "CityCenter";
	neighbors[] = {"Utes_CC03", "Utes_CC05"};
	demography[] = {CIV, 0, CIV_RU, 0};
	radiusA = 10;
	radiusB = 10;
	angle = 0;
};

class Utes_CC05 {
	name = "$STR_LOCATION_STRELKA";
	position[] = {13354.12, 1132.73};
	type = "CityCenter";
	neighbors[] = {"Utes_CC04"};
	demography[] = {CIV, 1, CIV_RU, 0};
	radiusA = 10;
	radiusB = 10;
	angle = 0;
};

class Utes_FAreaCS103 {
	name = "";
	position[] = {12444.12, 2265.73};
	type = "FlatAreaCitySmall";
	radiusA = 20;
	radiusB = 20;
	angle = 0;
};

class Utes_FArea06 {
	name = "";
	position[] = {13051.12, 897.73};
	type = "FlatArea";
	radiusA = 97.61;
	radiusB = 69.41;
	angle = 0;
};