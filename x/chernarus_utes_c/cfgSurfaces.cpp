class CfgSurfaces
{
	class Default;
	class CRGrass1: Default
	{
		access = 2;
		files = "cr_trava1_*";
		rough = 0.08;
		maxSpeedCoef = 0.9;
		dust = 0.15;
		lucidity = 4;
		grassCover = 0.05;
		soundEnviron = "grass";
		character = "CRGrassClutter";
		soundHit = "soft_ground";
		impact = "hitGroundSoft";
		surfaceFriction = 1.7;
	};
	class CRGrass2: Default
	{
		access = 2;
		files = "cr_trava2_*";
		rough = 0.08;
		maxSpeedCoef = 0.9;
		dust = 0.15;
		lucidity = 4;
		grassCover = 0.05;
		soundEnviron = "grass";
		character = "CRTallGrassClutter";
		soundHit = "soft_ground";
		impact = "hitGroundSoft";
		surfaceFriction = 1.7;
	};
	class CRGrassW1: Default
	{
		access = 2;
		files = "cr_travad1_*";
		rough = 0.08;
		dust = 0.15;
		lucidity = 4;
		grassCover = 0.05;
		soundEnviron = "grass";
		character = "CRGrassWClutter";
		soundHit = "soft_ground";
		impact = "hitGroundSoft";
		surfaceFriction = 1.7;
	};
	class CRGrassW2: Default
	{
		access = 2;
		files = "cr_travad2_*";
		rough = 0.08;
		dust = 0.15;
		lucidity = 4;
		grassCover = 0.05;
		soundEnviron = "grass";
		character = "CRTallGrassWClutter";
		soundHit = "soft_ground";
		impact = "hitGroundSoft";
		surfaceFriction = 1.7;
	};
	class CRForest1: Default
	{
		access = 2;
		files = "cr_les1_*";
		rough = 0.12;
		maxSpeedCoef = 0.8;
		dust = 0.4;
		lucidity = 3.5;
		grassCover = 0.04;
		soundEnviron = "drygrass";
		character = "CRForestMixedClutter";
		soundHit = "soft_ground";
		impact = "hitGroundSoft";
		surfaceFriction = 1.7;
	};
	class CRForest2: Default
	{
		access = 2;
		files = "cr_les2_*";
		rough = 0.12;
		maxSpeedCoef = 0.8;
		dust = 0.4;
		lucidity = 3.5;
		grassCover = 0.04;
		soundEnviron = "drygrass";
		character = "CRForestFirClutter";
		soundHit = "soft_ground";
		impact = "hitGroundSoft";
		surfaceFriction = 1.7;
	};
	class CRMudGround: Default
	{
		access = 2;
		files = "cr_pole_*";
		rough = 0.15;
		maxSpeedCoef = 0.85;
		dust = 0.32;
		lucidity = 1.5;
		grassCover = 0.04;
		soundEnviron = "dirt";
		character = "SparseWeedsClutter";
		soundHit = "soft_ground";
		impact = "hitGroundSoft";
		surfaceFriction = 1.7;
	};
	class CRField1: Default
	{
		access = 2;
		files = "cr_oranice_*";
		rough = 0.1;
		maxSpeedCoef = 0.9;
		dust = 0.33;
		lucidity = 1.5;
		grassCover = 0.04;
		soundEnviron = "dirt";
		character = "Empty";
		soundHit = "soft_ground";
		impact = "hitGroundSoft";
		surfaceFriction = 1.7;
	};
	class CRField2: Default
	{
		access = 2;
		files = "cr_strniste_*";
		rough = 0.1;
		maxSpeedCoef = 0.9;
		dust = 0.33;
		lucidity = 1.5;
		grassCover = 0.05;
		soundEnviron = "dirt";
		character = "CRStubbleClutter";
		soundHit = "soft_ground";
		impact = "hitGroundSoft";
		surfaceFriction = 1.7;
	};
	class CRGrit1: Default
	{
		access = 2;
		files = "cr_sterk_*";
		rough = 0.05;
		dust = 0.25;
		lucidity = 0.3;
		grassCover = 0;
		soundEnviron = "gravel";
		character = "Empty";
		soundHit = "hard_ground";
		impact = "hitGroundHard";
	};
	class CRHeather: Default
	{
		access = 2;
		files = "cr_vres_*";
		rough = 0.14;
		dust = 0.1;
		soundEnviron = "drygrass";
		character = "CRHeatherClutter";
		soundHit = "soft_ground";
		impact = "hitGroundSoft";
	};
	class CRRock: Default
	{
		access = 2;
		files = "cr_skala_*";
		rough = 0.2;
		maxSpeedCoef = 0.8;
		dust = 0.07;
		lucidity = 1;
		grassCover = 0;
		soundEnviron = "rock";
		character = "Empty";
		soundHit = "hard_ground";
		impact = "hitGroundHard";
		surfaceFriction = 1.9;
	};
	class CRTarmac: Default
	{
		access = 2;
		files = "cr_asfalt_*";
		rough = 0.05;
		dust = 0.02;
		lucidity = 0.5;
		soundEnviron = "gravel";
		character = "Empty";
		soundHit = "hard_ground";
		surfaceFriction = 2.5;
		impact = "hitGroundHard";
	};
	class CRConcrete: Default
	{
		access = 2;
		files = "cr_beton_*";
		rough = 0.05;
		dust = 0.08;
		lucidity = 0.3;
		soundEnviron = "concrete";
		character = "Empty";
		soundHit = "concrete";
		impact = "hitConcrete";
	};
};
