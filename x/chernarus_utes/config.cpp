class CfgPatches
{
	class Chernarus_Utes
	{
		units[] = {"Chernarus_Utes"};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"Chernarus","CUP_Chernarus_Config","Utes","CUP_Utes_Config"};
	};
};
